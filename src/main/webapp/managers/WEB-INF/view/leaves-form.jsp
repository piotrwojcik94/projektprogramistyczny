<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="tag" uri="/WEB-INF/taglibs/customTaglib.tld"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<html>
<head>
    <title>Urlopy</title>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.css" rel="stylesheet"/>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.css" rel="stylesheet"/>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js" ></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.js" type="text/javascript" ></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/moment.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.4.0/fullcalendar.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.9.0/locale/pl.js"></script>
    <link type="text/css" rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.4.0/fullcalendar.min.css" />
    <link href="/css/main.css" rel="stylesheet" media="screen">
    <link href="/css/settings.css" rel="stylesheet" media="screen">
    <script src="/js/main.js" type="text/javascript" media="screen"></script>
    <script src="/js/manager-leaves.js" type="text/javascript" media="screen"></script>
    <script src="/js/counting-new-leaves.js" type="text/javascript" media="screen"></script>
</head>
<body>
<div class="wrapper">
    <nav id="sidebar">
        <div class="sidebar-header">
            <h4>Zalogowany jako: <security:authentication property="principal.username"/></h4>
            <div class="dropdown">
                <div class="setting">
                    <h4 style="display: inline;"><span class="glyphicon glyphicon-user"></span></h4>
                    Ustawienia
                </div>
                <div class="dropdown-content">
                    <a href="${pageContext.request.contextPath}/managers/changeLogin">Zmień login</a>
                    <a href="${pageContext.request.contextPath}/managers/changePassword">Zmień hasło</a>
                </div>
            </div>
        </div>

        <ul class="list-unstyled components">
            <p><b>Opcje użytkownika</b></p>
            <li>
                <a href="${pageContext.request.contextPath}/managers/schedule">Pokaż grafik</a>
            </li>
            <li>
                <a href="${pageContext.request.contextPath}/managers/showFormForAddTask">Dodaj zadanie</a>
            </li>
            <li>
                <a href="${pageContext.request.contextPath}/managers/taskList">Pokaż zadania</a>
            </li>
            <li>
                <a href="${pageContext.request.contextPath}/managers/leaveList">Urlopy<span class="badge badge-pill badge-primary" id="newLeavesCounter" style="display: none;"></span></a>
            </li>
            <li>
                <a href="${pageContext.request.contextPath}/managers/wholeHistory">Pokaż historię pracy</a>
            </li>
        </ul>

        <ul class="list-unstyled CTAs">
            <li><form:form action="${pageContext.request.contextPath}/logout" method="POST">
                <input type="submit" value="Wyloguj się" class="btn btn-danger btn-block">
            </form:form>
            </li>
        </ul>
    </nav>
    <div id="content">

        <nav id="navbar" class="navbar navbar-default">
            <div class="content-fluid">

                <div class="navbar-header">
                    <button type="button" id="sidebarCollapse" class="btn btn-info navbar-btn">
                        <i class="glyphicon glyphicon-align-left"></i>
                    </button>
                </div>

                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right">
                        <li><form:form action="${pageContext.request.contextPath}/logout" method="POST">
                            <button type="submit" class="btn btn-danger btn-block">
                                <span class="glyphicon glyphicon-off"></span> Wyloguj się
                            </button>
                        </form:form></li>
                    </ul>
                </div>
            </div>
        </nav>
        <div class="container">
            <h3>Podania o urlop</h3>
            <hr>
            <c:choose>
                <c:when test="${leaves.size()==0}">
                    <h4>Brak podań o urlop!</h4>
                </c:when>
                <c:otherwise>
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>Nr</th>
                            <th>Pracownik</th>
                            <th>Początek urlopu</th>
                            <th>Koniec urlopu</th>
                            <th>Status</th>
                            <th>Operacje</th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach var="tempLeave" items="${leaves}" varStatus="i">
                            <c:choose>
                                <c:when test="${tempLeave.stateByStateId.stateId==6}">
                                    <tr class="info" style="font-weight: bold;">
                                </c:when>
                                <c:when test="${tempLeave.stateByStateId.stateId==7}">
                                    <tr class="danger">
                                </c:when>
                                <c:when test="${tempLeave.stateByStateId.stateId==8}">
                                    <tr class="success">
                                </c:when>
                            </c:choose>
                            <td>${offset + i.index + 1}</td>
                            <td>${tempLeave.userByUserId.firstName} ${tempLeave.userByUserId.lastName}</td>
                            <td>${tempLeave.dateFrom}</td>
                            <td>${tempLeave.dateTo}</td>
                            <td>${tempLeave.stateByStateId.state}</td>
                            <td>
                                <c:choose>
                                    <c:when test="${tempLeave.userByUserId.roleByRoleId.roleId!=3}">
                                        <c:choose>
                                        <c:when test="${tempLeave.stateByStateId.stateId==7}">
                                            <c:url var="kickLink" value="/managers/kickLeave">
                                                <c:param name="leaveId" value="${tempLeave.leaveId}"></c:param>
                                            </c:url>
                                            <a disabled class="btn btn-danger">
                                                Odrzuć
                                            </a>
                                            <c:url var="acceptLink" value="/managers/acceptLeave">
                                                <c:param name="leaveId" value="${tempLeave.leaveId}"></c:param>
                                            </c:url>
                                            <a href="${acceptLink}" class="btn btn-success"
                                               onclick="if (!(confirm('Urlop zostanie zaakcpetowany'))) return false">
                                                Akceptuj
                                            </a>
                                        </c:when>
                                        <c:when test="${tempLeave.stateByStateId.stateId==8}">
                                            <c:url var="kickLink" value="/managers/kickLeave">
                                                <c:param name="leaveId" value="${tempLeave.leaveId}"></c:param>
                                            </c:url>
                                            <a href="${kickLink}" class="btn btn-danger"
                                               onclick="if (!(confirm('Urlop zostanie odrzucony'))) return false">
                                                Odrzuć
                                            </a>
                                            <c:url var="acceptLink" value="/managers/acceptLeave">
                                                <c:param name="leaveId" value="${tempLeave.leaveId}"></c:param>
                                            </c:url>
                                            <a disabled class="btn btn-success">
                                                Akceptuj
                                            </a>
                                        </c:when>
                                        <c:when test="${tempLeave.stateByStateId.stateId==6}">
                                            <c:url var="kickLink" value="/managers/kickLeave">
                                                <c:param name="leaveId" value="${tempLeave.leaveId}"></c:param>
                                            </c:url>
                                            <a href="${kickLink}" class="btn btn-danger"
                                               onclick="if (!(confirm('Urlop zostanie odrzucony'))) return false">
                                                Odrzuć
                                            </a>
                                            <c:url var="acceptLink" value="/managers/acceptLeave">
                                                <c:param name="leaveId" value="${tempLeave.leaveId}"></c:param>
                                            </c:url>
                                            <a href="${acceptLink}" class="btn btn-success"
                                               onclick="if (!(confirm('Urlop zostanie zaakcpetowany'))) return false">
                                                Akceptuj
                                            </a>
                                        </c:when>
                                        </c:choose>
                                    </c:when>
                                </c:choose>
                                <c:url var="deleteLink" value="/managers/deleteLeave">
                                    <c:param name="leaveId" value="${tempLeave.leaveId}"></c:param>
                                </c:url>
                                <a href="${deleteLink}" class="btn btn-warning"
                                   onclick="if (!(confirm('Czy na pewno chcesz usunać podanie o urlop?'))) return false">
                                    <span class="glyphicon glyphicon-remove"></span>
                                    Usuń
                                </a>
                            </td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                    <tag:paginate max="15" offset="${offset}" count="${count}"
                                  uri="/managers/leaveList" next="&raquo;" previous="&laquo;" />
                </c:otherwise>
            </c:choose>

            <h3>Wpisz urlop</h3>
            <hr>
            <form:form id="newLeave" action="${pageContext.request.contextPath}/managers/saveLeave" modelAttribute="leave" method="POST">
                <form:hidden id="dateFrom" path="dateFrom"></form:hidden>
                <form:hidden id="dateTo" path="dateTo"></form:hidden>
                <form:hidden id="stateId" path="stateByStateId.stateId"></form:hidden>
            </form:form>
            <div id='calendar'></div>
            <hr>
            <button onclick="window.location.href='/managers'" type="button" class="btn btn-primary">Wstecz</button>
        </div>
    </div>
</div>
</body>
</html>