<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<html>
    <head>
        <title>Grafik</title>
        <meta charset="UTF-8"/>

        <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.css"
              rel="stylesheet"/>
        <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.css"
              rel="stylesheet"/>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.js"
                type="text/javascript"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
        <link href="/css/main.css" rel="stylesheet" media="screen">
        <link href="/css/settings.css" rel="stylesheet" media="screen">
        <script src="/js/main.js" type="text/javascript" media="screen"></script>
        <script src="/js/counting-new-leaves.js" type="text/javascript" media="screen"></script>
    </head>
    <body>
        <div class="wrapper">
            <nav id="sidebar">
                <div class="sidebar-header">
                    <h4>Zalogowany jako: <security:authentication
                            property="principal.username"/></h4>
                    <div class="dropdown">
                        <div class="setting">
                            <h4 style="display: inline;"><span class="glyphicon glyphicon-user"></span></h4>
                            Ustawienia
                        </div>
                        <div class="dropdown-content">
                            <a href="${pageContext.request.contextPath}/managers/changeLogin">Zmień login</a>
                            <a href="${pageContext.request.contextPath}/managers/changePassword">Zmień hasło</a>
                        </div>
                    </div>
                </div>

                <ul class="list-unstyled components">
                    <p><b>Opcje użytkownika</b></p>
                    <li>
                        <a href="${pageContext.request.contextPath}/managers/schedule">Pokaż grafik</a>
                    </li>
                    <li>
                        <a href="${pageContext.request.contextPath}/managers/showFormForAddTask">Dodaj zadanie</a>
                    </li>
                    <li>
                        <a href="${pageContext.request.contextPath}/managers/taskList">Pokaż zadania</a>
                    </li>
                    <li>
                        <a href="${pageContext.request.contextPath}/managers/leaveList">Urlopy<span class="badge badge-pill badge-primary" id="newLeavesCounter" style="display: none;"></span></a>
                    </li>
                    <li>
                        <a href="${pageContext.request.contextPath}/managers/wholeHistory">Pokaż historię pracy</a>
                    </li>
                </ul>

                <ul class="list-unstyled CTAs">
                    <li><form:form action="${pageContext.request.contextPath}/logout" method="POST">
                        <input type="submit" value="Wyloguj się" class="btn btn-danger btn-block">
                    </form:form>
                    </li>
                </ul>
            </nav>
            <div id="content">

                <nav id="navbar" class="navbar navbar-default">
                    <div class="content-fluid">

                        <div class="navbar-header">
                            <button type="button" id="sidebarCollapse"
                                    class="btn btn-info navbar-btn">
                                <i class="glyphicon glyphicon-align-left"></i>
                            </button>
                        </div>

                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <ul class="nav navbar-nav navbar-right">
                                <li><form:form action="${pageContext.request.contextPath}/logout"
                                               method="POST">
                                    <button type="submit" class="btn btn-danger btn-block">
                                        <span class="glyphicon glyphicon-off"></span> Wyloguj się
                                    </button>
                                </form:form></li>
                            </ul>
                        </div>
                    </div>
                </nav>

                <div class="container">
                    <h2>Orientacyjny grafik</h2>


                    <c:choose>
                        <c:when test="${scheduleByUser.size()==0}">
                            <h4>Pusty grafik</h4>
                        </c:when>
                        <c:otherwise>

                            <table border="1" cellpadding="5" class="table table-hover">
                                <thead style="text-align: center; font-weight: bold">
                                <tr>
                                    <td rowspan="3">Imię i nazwisko</td>
                                    <td colspan="10">Dzień tygodnia</td>
                                </tr>
                                <tr>
                                    <td colspan="2">Poniedziałek</td>
                                    <td colspan="2">Wtorek</td>
                                    <td colspan="2">Środa</td>
                                    <td colspan="2">Czwartek</td>
                                    <td colspan="2">Piątek</td>
                                </tr>
                                <tr>
                                    <c:forEach begin="1" end="5">
                                        <td>Od</td>
                                        <td>Do</td>
                                    </c:forEach>
                                </tr>
                                </thead>
                                <tbody>
                                <c:forEach var="scheduleMap" items="${scheduleByUser}">
                                    <c:set var="user" value="${scheduleMap.key}"/>
                                    <c:set var="scheduleList" value="${scheduleMap.value}"/>

                                    <tr>
                                        <td style="font-weight: bold">${user.firstName} ${user.lastName}</td>
                                        <c:forEach var="schedule" items="${scheduleList}">
                                            <td>${schedule.enterHour}</td>
                                            <td>${schedule.exitHour}</td>
                                        </c:forEach>
                                    </tr>
                                </c:forEach>
                                </tbody>
                            </table>
                        </c:otherwise>
                    </c:choose>

                    <br/>
                    <c:url var="editLink" value="/managers/formForAddSchedule">
                        <c:param name="userId" value="${currentUser.userId}"/>
                    </c:url>
                    <input type="button" value="Edytuj swój grafik"
                           onclick="window.location.href='${editLink}'; return false;"/>

                    <br/>
                    <p>
                        <a href="${pageContext.request.contextPath}/">Strona główna</a>
                    </p>
                </div>
            </div>
        </div>
    </body>
</html>
