<%--
  Created by IntelliJ IDEA.
  User: Piotr
  Date: 03.04.2018
  Time: 13:07
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<html>
<head>
    <title>Szczegóły zadania</title>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.css" rel="stylesheet"/>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.css" rel="stylesheet"/>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js" ></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.js" type="text/javascript" ></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
    <link href="/css/main.css" rel="stylesheet" media="screen">
    <link href="/css/settings.css" rel="stylesheet" media="screen">
    <script src="/js/main.js" type="text/javascript" media="screen"></script>
    <script src="/js/date-time-picker.js" type="text/javascript" media="screen"></script>
    <script src="/js/counting-new-leaves.js" type="text/javascript" media="screen"></script>
</head>
<body>
<div class="wrapper">
    <nav id="sidebar">
        <div class="sidebar-header">
            <h4>Zalogowany jako: <security:authentication property="principal.username"/></h4>
            <div class="dropdown">
                <div class="setting">
                    <h4 style="display: inline;"><span class="glyphicon glyphicon-user"></span></h4>
                    Ustawienia
                </div>
                <div class="dropdown-content">
                    <a href="${pageContext.request.contextPath}/managers/changeLogin">Zmień login</a>
                    <a href="${pageContext.request.contextPath}/managers/changePassword">Zmień hasło</a>
                </div>
            </div>
        </div>

        <ul class="list-unstyled components">
            <p><b>Opcje użytkownika</b></p>
            <li>
                <a href="${pageContext.request.contextPath}/managers/schedule">Pokaż grafik</a>
            </li>
            <li>
                <a href="${pageContext.request.contextPath}/managers/showFormForAddTask">Dodaj zadanie</a>
            </li>
            <li>
                <a href="${pageContext.request.contextPath}/managers/taskList">Pokaż zadania</a>
            </li>
            <li>
                <a href="${pageContext.request.contextPath}/managers/leaveList">Urlopy<span class="badge badge-pill badge-primary" id="newLeavesCounter" style="display: none;"></span></a>
            </li>
            <li>
                <a href="${pageContext.request.contextPath}/managers/wholeHistory">Pokaż historię pracy</a>
            </li>
        </ul>

        <ul class="list-unstyled CTAs">
            <li><form:form action="${pageContext.request.contextPath}/logout" method="POST">
                <input type="submit" value="Wyloguj się" class="btn btn-danger btn-block">
            </form:form>
            </li>
        </ul>
    </nav>
    <div id="content">

        <nav id="navbar" class="navbar navbar-default">
            <div class="content-fluid">

                <div class="navbar-header">
                    <button type="button" id="sidebarCollapse" class="btn btn-info navbar-btn">
                        <i class="glyphicon glyphicon-align-left"></i>
                    </button>
                </div>

                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right">
                        <li><form:form action="${pageContext.request.contextPath}/logout" method="POST">
                            <button type="submit" class="btn btn-danger btn-block">
                                <span class="glyphicon glyphicon-off"></span> Wyloguj się
                            </button>
                        </form:form></li>
                    </ul>
                </div>
            </div>
        </nav>
        <div class="container" style="padding-bottom: 30px;">
            <h2>Szczegóły zadania</h2>
            <hr>
            <form:form action="/managers/updateTask" modelAttribute="task" method="post" class="form-horizontal">
                <form:hidden path="taskId"/>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="task">Zadanie:</label>
                    <div class="col-sm-10">
                        <form:input path="task" type="text" class="form-control" id="task" placeholder="Wprowadź nazwę zadania" name="task"/>
                        <form:errors path="task" cssClass="error" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="description">Opis:</label>
                    <div class="col-sm-10">
                        <form:input path="description" type="text" class="form-control" id="description" placeholder="Wprowadź opis zadania" name="description"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="startDate">Data rozpoczęcia:</label>
                    <div class='input-group date col-sm-10' id='datetimepicker1' style="padding-left: 15px; max-width: 82%">
                        <form:input path="startDate" type='text' placeholder="${task.startDate}" class="form-control" id="startDate" name="startDate"/>
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="endDate">Data zakończenia:</label>
                    <div class='input-group date col-sm-10' id='datetimepicker2' style="padding-left: 15px; max-width: 82%">
                        <form:input path="endDate" type='text' placeholder="${task.startDate}" class="form-control" id="endDate" name="endDate"/>
                        <form:errors path="endDate" cssClass="error" />
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="status">Status:</label>
                    <div class="col-sm-10">
                        <form:select path="stateByStateId.stateId" class="form-control" id="status" name="status">
                            <c:forEach var="tempState" items="${states}">
                                <form:option value="${tempState.stateId}">
                                    ${tempState.state}
                                </form:option>
                            </c:forEach>
                        </form:select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="user">Wykonawca:</label>
                    <div class="col-sm-10">
                        <form:select path="userByUserId.userId" class="form-control" id="user" name="user">
                            <form:option value="${task.userByUserId.userId}" selected="true">${task.userByUserId.firstName} ${task.userByUserId.lastName}</form:option>
                            <c:forEach var="tempUser" items="${users}">
                                <form:option value="${tempUser.userId}">
                                    ${tempUser.firstName} ${tempUser.lastName}
                                </form:option>
                            </c:forEach>
                        </form:select>
                    </div>
                </div>
                <div class="form-group">
                    <div class="control-label col-sm-2"></div>
                    <div class="col-sm-10">
                        <button type="submit" class="btn btn-success btn-block">Aktualizuj</button>
                    </div>
                </div>
            </form:form>

            <h3>Komentarze do zadania</h3>
            <hr>
            <form:form action="/managers/addComment" modelAttribute="comment" method="post" class="form-horizontal">
                <div class="form-group">
                    <label class="control-label col-sm-2" for="cont">Nowy komentarz:</label>
                    <div class="col-sm-10">
                        <form:input path="content" type="text" class="form-control" id="cont" placeholder="Napisz komentarz..." name="cont"/>
                        <form:errors path="content" cssClass="error" />
                        <form:hidden path="taskByTaskId.taskId"/>
                    </div>
                </div>
                <div class="form-group">
                    <div class="control-label col-sm-2"></div>
                    <div class="col-sm-10">
                        <button type="submit" class="btn btn-success btn-block">Dodaj komentarz</button>
                    </div>
                </div>
                <hr>
            </form:form>
            <c:choose>
                <c:when test="${comments.size()==0}">
                    <div class="control-label col-sm-2"></div>
                    <h4>Brak komentarzy do zadania</h4>
                </c:when>
                <c:otherwise>
                    <div class="control-label col-sm-2"></div>
                    <div class="col-sm-10">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>Komentarze</th>
                            </tr>
                            </thead>
                            <tbody>
                            <c:forEach var="tempComment" items="${comments}">
                            <tr>
                                <td>
                                    <div style="padding: 5px;">Użytkownik: <b>${tempComment.userByUserId.firstName} ${tempComment.userByUserId.lastName}</b></div>
                                    <div style="padding: 5px;">${tempComment.content}</div>
                                </td>
                                <td style="text-align: right;padding: 5px;">Data dodania: <b>${tempComment.commentDate}</b></td>
                            </tr>
                            </c:forEach>
                        </table>
                    </div>
                </c:otherwise>
            </c:choose>
            <button onclick="window.location.href='/managers/taskList'" type="button" class="btn btn-primary">Wstecz</button>
        </div>
</div>
</body>
</html>