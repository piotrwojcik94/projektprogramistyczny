<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="tag" uri="/WEB-INF/taglibs/customTaglib.tld"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<html>
<head>
    <title>Dodane zadania</title>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.css" rel="stylesheet"/>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.css" rel="stylesheet"/>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js" ></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.js" type="text/javascript" ></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
    <link href="/css/main.css" rel="stylesheet" media="screen">
    <link href="/css/settings.css" rel="stylesheet" media="screen">
    <script src="/js/main.js" type="text/javascript" media="screen"></script>
    <script src="/js/counting-new-tasks.js" type="text/javascript" media="screen"></script>
</head>
<body>
<div class="wrapper">
    <nav id="sidebar">
        <div class="sidebar-header">
            <h4>Zalogowany jako: <security:authentication property="principal.username"/></h4>
            <div class="dropdown">
                <div class="setting">
                    <h4 style="display: inline;"><span class="glyphicon glyphicon-user"></span></h4>
                    Ustawienia
                </div>
                <div class="dropdown-content">
                    <a href="${pageContext.request.contextPath}/employees/changeLogin">Zmień login</a>
                    <a href="${pageContext.request.contextPath}/employees/changePassword">Zmień hasło</a>
                </div>
            </div>
        </div>

        <ul class="list-unstyled components">
            <p><b>Opcje użytkownika</b></p>
            <li>
                <a href="${pageContext.request.contextPath}/employees/schedule">Pokaż grafik</a>
            </li>
            <li>
                <a href="${pageContext.request.contextPath}/employees/taskList">Pokaż zadania<span class="badge badge-pill badge-primary" id="newTasksCounter" style="display: none;"></span></a>
            </li>
            <li>
                <a href="${pageContext.request.contextPath}/employees/leaveList">Urlopy</a>
            </li>
            <li>
                <a href="${pageContext.request.contextPath}/employees/wholeHistoryForUser">Pokaż historię pracy</a>
            </li>
        </ul>

        <ul class="list-unstyled CTAs">
            <li><form:form action="${pageContext.request.contextPath}/logout" method="POST">
                <input type="submit" value="Wyloguj się" class="btn btn-danger btn-block">
            </form:form>
            </li>
        </ul>
    </nav>
    <div id="content">

        <nav id="navbar" class="navbar navbar-default">
            <div class="content-fluid">

                <div class="navbar-header">
                    <button type="button" id="sidebarCollapse" class="btn btn-info navbar-btn">
                        <i class="glyphicon glyphicon-align-left"></i>
                    </button>
                </div>

                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right">
                        <li><form:form action="${pageContext.request.contextPath}/logout" method="POST">
                            <button type="submit" class="btn btn-danger btn-block">
                                <span class="glyphicon glyphicon-off"></span> Wyloguj się
                            </button>
                        </form:form></li>
                    </ul>
                </div>
            </div>
        </nav>
        <div class="container">
            <h2>Lista zadań</h2>
            <hr>
            <c:choose>
                <c:when test="${tasks.size()==0}">
                    <h3>Brak zdefiniowanych zadań dla pracownika!</h3>
                </c:when>
                <c:otherwise>
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>Nr</th>
                            <th>Nazwa Zadania</th>
                            <th>Początek</th>
                            <th>Koniec</th>
                            <th>Od kogo</th>
                            <th>Więcej</th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach var="tempTask" items="${tasks}" varStatus="i">
                            <c:choose>
                                <c:when test="${tempTask.showed == false}">
                                    <tr style="font-weight: bold;">
                                </c:when>
                                <c:otherwise>
                                    <tr style="font-weight: normal;">
                                </c:otherwise>
                            </c:choose>
                                <td>${offset + i.index + 1}</td>
                                <td>${tempTask.task}</td>
                                <td>${tempTask.startDate}</td>
                                <td>${tempTask.endDate}</td>
                                <td>${tempTask.userByAuthorId.firstName} ${tempTask.userByAuthorId.lastName}</td>
                                <td>
                                    <c:url var="detailsLink" value="/employees/showTaskDetails">
                                        <c:param name="taskId" value="${tempTask.taskId}"></c:param>
                                    </c:url>
                                    <button onclick="window.location.href='${detailsLink}'" type="button" class="btn btn-info">
                                        Szczegóły
                                    </button>
                                </td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                    <tag:paginate max="15" offset="${offset}" count="${count}"
                                  uri="/employees/taskList" next="&raquo;" previous="&laquo;" />
                </c:otherwise>
            </c:choose>
            <hr>
            <button onclick="window.location.href='/employees'" type="button" class="btn btn-primary">Wstecz</button>
        </div>
    </div>
</div>
</body>
</html>