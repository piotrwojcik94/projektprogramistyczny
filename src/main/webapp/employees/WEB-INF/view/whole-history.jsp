<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<html>
    <head>
        <title>Historia</title>
        <meta charset="UTF-8"/>

        <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.css"
              rel="stylesheet"/>
        <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.css"
              rel="stylesheet"/>
        <link href="../../../css/fullcalendar/fullcalendar.css"
              rel="stylesheet"/>
        <link href='../../../css/fullcalendar/fullcalendar.print.css'
              rel='stylesheet' media='print'/>
        <link href='../../../css/fullcalendar/scheduler.css'
              rel='stylesheet'/>
        <link href="/css/main.css" rel="stylesheet" media="screen"/>
        <link href="/css/settings.css" rel="stylesheet" media="screen">
        <script src="/js/main.js" type="text/javascript" media="screen"></script>
        <script src="/js/counting-new-tasks.js" type="text/javascript" media="screen"></script>

        <script src="../../../js/jquery.js"></script>
        <script src="../../../js/moment.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
        <script src="../../../js/fullcalendar/fullcalendar.js"></script>
        <script src="../../../js/fullcalendar/locale-all.js"></script>
        <script src="../../../js/fullcalendar/scheduler.js"></script>
        <script src="../../../js/fullcalendar/gcal.js"></script>

        <script>
            var _workDays = {};
            var _displayTimeSemafor = 0;

            function init() {
                _displayTimeSemafor = 0;
                _workDays = {};
            }

            function addTime(startParam, endParam) {
                var start = startParam;
                var end = endParam;
                var a = start.split(":");
                var seconds = (+a[0]) * 60 * 60 + (+a[1]) * 60 + (+a[2]);
                var b = end.split(":");
                var seconds2 = (+b[0]) * 60 * 60 + (+b[1]) * 60 + (+b[2]);

                var date = new Date(1970, 0, 1);
                date.setSeconds(seconds + seconds2);

                return date.toTimeString().replace(/.*(\d{2}:\d{2}:\d{2}).*/, "$1");
            };

            function subtractTime(startParam, endParam) {
                var start = startParam;
                var end = endParam;
                var a = start.split(":");
                var seconds = (+a[0]) * 60 * 60 + (+a[1]) * 60 + (+a[2]);
                var b = end.split(":");
                var seconds2 = (+b[0]) * 60 * 60 + (+b[1]) * 60 + (+b[2]);

                return secondsToHHMMSS(seconds - seconds2);
            };

            function displayDayTime() {
                if (_displayTimeSemafor === 0) {
                    _displayTimeSemafor = 1;
                    Object.keys(_workDays).forEach(function (key, index) {
                        var element = $("[data-date='" + key + "']");
                        element.append('Ogólny czas: <span style="color: red">' + _workDays[key] + '</span><br />');
                    });
                }
            };

            function displayTimeToEnd(month, year) {
                var element = $("#timeToEnd");
                var workingTime = getDaysInMonth(month, year) * 8 + ':00:00';

                element.html('Czas pracy w tym miesiącu: <span style="color: darkcyan">' + workingTime + '</span><br />')

                Object.keys(_workDays).forEach(function (key, index) {
                    workingTime = subtractTime(workingTime, _workDays[key]);
                });

                element.append('Czas pozostały do wypracowania całego etatu: <span style="color: red">' + workingTime + '</span>');
            };

            // function getDaysInMonth(month, year) {
            //     month--; // lets fix the month once, here and be done with it
            //     var date = new Date(year, month, 1);
            //
            //     var days = [];
            //     while (date.getMonth() === month) {
            //
            //         // Exclude weekends
            //         var tmpDate = new Date(date);
            //         var weekDay = tmpDate.getDay(); // week day
            //         var day = tmpDate.getDate(); // day
            //
            //         if (weekDay % 6) { // exclude 0=Sunday and 6=Saturday
            //             days.push(day);
            //         }
            //
            //         date.setDate(date.getDate() + 1);
            //     }
            //
            //     return days.length;
            // };

            function getDaysInMonth(month, year) {
                var startDate = new Date(year, month, 1);
                var endDate = new Date(year, month + 1, 0);

                var count = 0;
                var curDate = startDate;
                while (curDate <= endDate) {
                    var dayOfWeek = curDate.getDay();
                    if(!((dayOfWeek === 6) || (dayOfWeek === 0)))
                        count++;
                    curDate.setDate(curDate.getDate() + 1);
                }
                return count;
            };

            function secondsToHHMMSS(seconds) {
                return (Math.floor(seconds / 3600)) + ":" + ("0" + Math.floor(seconds / 60) % 60).slice(-2) + ":" + ("0" + seconds % 60).slice(-2)
            }

            $(document).ready(function () {
                $('#calendar').fullCalendar({
                    header: {
                        left: 'prev,next today',
                        center: 'title',
                        right: ''
                    },
                    schedulerLicenseKey: 'GPL-My-Project-Is-Open-Source',
                    navLinks: true,
                    // eventLimit: true,
                    defaultView: 'month',
                    showNonCurrentDates: true,
                    fixedWeekCount: false,
                    locale: 'pl',
                    height: 500,
                    weekends: false,
                    // defaultTimedEventDuration: 1,
                    displayEventTime: false,
                    // displayEventEnd: true,
                    events: 'http://localhost:8080/historyJsonByUser',

                    eventRender: function (event, element) {
                        element.find(".fc-title").remove();

                        var begin = moment(event.start).format("HH:mm:ss");
                        var end = moment(event.stop).format("HH:mm:ss");

                        var new_description =
                            'Od <strong>' + begin + '</strong> do <strong>' + end + '</strong><br/>'
                            + 'Czas: <strong>' + event.timeDifference + '</strong><br/>';

                        element.append(new_description);
                    },

                    eventAfterRender: function (event, element, view) {
                        var day = event.stop.split(" ");

                        if (typeof _workDays[day[0]] === "undefined")
                            _workDays[day[0]] = event.timeDifference;
                        else
                            _workDays[day[0]] = addTime(_workDays[day[0]], event.timeDifference);
                    },

                    eventAfterAllRender: function (view) {
                        displayDayTime();

                        var date = $('#calendar').fullCalendar('getDate');

                        displayTimeToEnd(date.month(), date.year());
                    },

                    viewRender: function( view, element ) {
                        init();
                    }
                });
            });
        </script>

        <style>
            .fc-sat {
                background: steelblue !important;
            }

            .fc-sun {
                background: indianred !important;
            }
        </style>
    </head>
    <body>
        <div class="wrapper">
            <nav id="sidebar">
                <div class="sidebar-header">
                    <h4>Zalogowany jako: <security:authentication
                            property="principal.username"/></h4>
                    <div class="dropdown">
                        <div class="setting">
                            <h4 style="display: inline;"><span class="glyphicon glyphicon-user"></span></h4>
                            Ustawienia
                        </div>
                        <div class="dropdown-content">
                            <a href="${pageContext.request.contextPath}/employees/changeLogin">Zmień login</a>
                            <a href="${pageContext.request.contextPath}/employees/changePassword">Zmień hasło</a>
                        </div>
                    </div>
                </div>

                <ul class="list-unstyled components">
                    <p><b>Opcje użytkownika</b></p>
                    <li>
                        <a href="${pageContext.request.contextPath}/employees/schedule">Pokaż grafik</a>
                    </li>
                    <li>
                        <a href="${pageContext.request.contextPath}/employees/taskList">Pokaż zadania<span class="badge badge-pill badge-primary" id="newTasksCounter" style="display: none;"></span></a>
                    </li>
                    <li>
                        <a href="${pageContext.request.contextPath}/employees/leaveList">Urlopy</a>
                    </li>
                    <li>
                        <a href="${pageContext.request.contextPath}/employees/wholeHistoryForUser">Pokaż historię pracy</a>
                    </li>
                </ul>

                <ul class="list-unstyled CTAs">
                    <li><form:form action="${pageContext.request.contextPath}/logout" method="POST">
                        <input type="submit" value="Wyloguj się" class="btn btn-danger btn-block">
                    </form:form>
                    </li>
                </ul>
            </nav>
            <div id="content">

                <nav id="navbar" class="navbar navbar-default">
                    <div class="content-fluid">

                        <div class="navbar-header">
                            <button type="button" id="sidebarCollapse"
                                    class="btn btn-info navbar-btn">
                                <i class="glyphicon glyphicon-align-left"></i>
                            </button>
                        </div>

                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <ul class="nav navbar-nav navbar-right">
                                <li><form:form action="${pageContext.request.contextPath}/logout"
                                               method="POST">
                                    <button type="submit" class="btn btn-danger btn-block">
                                        <span class="glyphicon glyphicon-off"></span> Wyloguj się
                                    </button>
                                </form:form></li>
                            </ul>
                        </div>
                    </div>
                </nav>

                <div class="container">
                    <h3>Historia</h3>
                    <div style="padding: 40px" id='calendar'></div>
                    <div>
                        <h3 id="timeToEnd"></h3>
                    </div>
                    <button onclick="window.location.href='/employees'" type="button"
                            class="btn btn-primary">Wstecz
                    </button>
                </div>
            </div>
        </div>
    </body>
</html>