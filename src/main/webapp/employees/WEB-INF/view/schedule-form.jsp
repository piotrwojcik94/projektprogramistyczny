<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<html>
<head>
    <title>Edytuj grafik</title>
    <meta charset="UTF-8"/>

    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.css" rel="stylesheet"/>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.css" rel="stylesheet"/>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js" ></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.js" type="text/javascript" ></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
    <link href="/css/main.css" rel="stylesheet" media="screen">
    <link href="/css/settings.css" rel="stylesheet" media="screen">
    <script src="/js/main.js" type="text/javascript" media="screen"></script>
    <script src="/js/counting-new-tasks.js" type="text/javascript" media="screen"></script>
</head>
<body>
<div class="wrapper">
    <nav id="sidebar">
        <div class="sidebar-header">
            <h4>Zalogowany jako: <security:authentication property="principal.username"/></h4>
            <div class="dropdown">
                <div class="setting">
                    <h4 style="display: inline;"><span class="glyphicon glyphicon-user"></span></h4>
                    Ustawienia
                </div>
                <div class="dropdown-content">
                    <a href="${pageContext.request.contextPath}/employees/changeLogin">Zmień login</a>
                    <a href="${pageContext.request.contextPath}/employees/changePassword">Zmień hasło</a>
                </div>
            </div>
        </div>

        <ul class="list-unstyled components">
            <p><b>Opcje użytkownika</b></p>
            <li>
                <a href="${pageContext.request.contextPath}/employees/schedule">Pokaż grafik</a>
            </li>
            <li>
                <a href="${pageContext.request.contextPath}/employees/taskList">Pokaż zadania<span class="badge badge-pill badge-primary" id="newTasksCounter" style="display: none;"></span></a>
            </li>
            <li>
                <a href="${pageContext.request.contextPath}/employees/leaveList">Urlopy</a>
            </li>
            <li>
                <a href="${pageContext.request.contextPath}/employees/wholeHistoryForUser">Pokaż historię pracy</a>
            </li>
        </ul>

        <ul class="list-unstyled CTAs">
            <li><form:form action="${pageContext.request.contextPath}/logout" method="POST">
                <input type="submit" value="Wyloguj się" class="btn btn-danger btn-block">
            </form:form>
            </li>
        </ul>
    </nav>
    <div id="content">

        <nav id="navbar" class="navbar navbar-default">
            <div class="content-fluid">

                <div class="navbar-header">
                    <button type="button" id="sidebarCollapse" class="btn btn-info navbar-btn">
                        <i class="glyphicon glyphicon-align-left"></i>
                    </button>
                </div>

                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right">
                        <li><form:form action="${pageContext.request.contextPath}/logout" method="POST">
                            <button type="submit" class="btn btn-danger btn-block">
                                <span class="glyphicon glyphicon-off"></span> Wyloguj się
                            </button>
                        </form:form></li>
                    </ul>
                </div>
            </div>
        </nav>

        <div class="container">
            <h2>Edytuj grafik</h2>

            <form:form action="/employees/saveSchedule" modelAttribute="scheduleForm" method="POST"
                       class="form-horizontal">

                <table cellpadding="10">
                    <thead style="font-weight: bold;">
                    <tr>
                        <td style="text-align: center; padding-bottom: 20px" class="control-label col-sm-1">Dzień tygodnia</td>
                        <td style="text-align: center;" class="control-label col-sm-1">Od</td>
                        <td style="text-align: center;" class="control-label col-sm-1">Do</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach begin="0" end="4" varStatus="loop">
                        <c:set var="index" value="${loop.index}"/>
                        <tr>
                            <form:hidden path="scheduleList[${index}].scheduleId"/>
                            <form:hidden path="scheduleList[${index}].weekDayByWeekDayId.dayId"
                                         value="${index+1}"/>
                            <td style="font-weight: bold; padding-bottom: 20px;">${workdays.get(index).dayPl}</td>
                            <td>
                                <div class='input-group date col-sm-10' id='datetimepicker${index}'>
                                    <form:input path="scheduleList[${index}].enterHour"
                                                placeholder="od" class="form-control"/>
                                    <span class="input-group-addon">
                                <span class="glyphicon glyphicon-time"></span>
                            </span>
                                </div>
                            </td>
                            <td>
                                <div class='input-group date col-sm-10'
                                     id='datetimepicker${index + 5}'>
                                    <form:input path="scheduleList[${index}].exitHour"
                                                placeholder="do" class="form-control"/>
                                    <span class="input-group-addon">
                                <span class="glyphicon glyphicon-time"></span>
                            </span>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td colspan="2"><form:errors path="scheduleList[${index}].exitHour" cssClass="error"/></td>
                        </tr>
                    </c:forEach>
                    <tr>
                        <td></td>
                        <td><input class="btn btn-success btn-block" type="submit" value="Zapisz"/></td>
                        <td></td>
                    </tr>

                    </tbody>

                </table>

            </form:form>

            <br/>
            <p>
                <a class="btn btn-primary" href="${pageContext.request.contextPath}/">Strona główna</a>
            </p>
        </div>
    </div>
</div>
<script>
    $(function () {
        for (var i = 0; i < 10; i++) {
            $('#datetimepicker' + i).datetimepicker({
                format: 'HH:mm:[00]',
                // defaultDate: null,  //TODO not working - why???
                // minDate: new Date(0, 0, 0, 6),
                // maxDate: new Date(0, 0, 0, 22),
                stepping: 10
            });

            // $('#scheduleList' + i + '.enterHour').val("");
            // $('#scheduleList' + i + '.exitHour').val("");
        }
    });
</script>
</body>
</html>