<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="tag" uri="/WEB-INF/taglibs/customTaglib.tld"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<html>
<head>
    <title>Dodaj urlop</title>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.css" rel="stylesheet"/>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.css" rel="stylesheet"/>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js" ></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.js" type="text/javascript" ></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/moment.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.4.0/fullcalendar.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.9.0/locale/pl.js"></script>
    <link type="text/css" rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.4.0/fullcalendar.min.css" />
    <link href="/css/main.css" rel="stylesheet" media="screen">
    <link href="/css/settings.css" rel="stylesheet" media="screen">
    <script src="/js/main.js" type="text/javascript" media="screen"></script>
    <script src="/js/counting-new-tasks.js" type="text/javascript" media="screen"></script>
</head>
<body>
<div class="wrapper">
    <nav id="sidebar">
        <div class="sidebar-header">
            <h4>Zalogowany jako: <security:authentication property="principal.username"/></h4>
            <div class="dropdown">
                <div class="setting">
                    <h4 style="display: inline;"><span class="glyphicon glyphicon-user"></span></h4>
                    Ustawienia
                </div>
                <div class="dropdown-content">
                    <a href="${pageContext.request.contextPath}/employees/changeLogin">Zmień login</a>
                    <a href="${pageContext.request.contextPath}/employees/changePassword">Zmień hasło</a>
                </div>
            </div>
        </div>

        <ul class="list-unstyled components">
            <p><b>Opcje użytkownika</b></p>
            <li>
                <a href="${pageContext.request.contextPath}/employees/schedule">Pokaż grafik</a>
            </li>
            <li>
                <a href="${pageContext.request.contextPath}/employees/taskList">Pokaż zadania<span class="badge badge-pill badge-primary" id="newTasksCounter" style="display: none;"></span></a>
            </li>
            <li>
                <a href="${pageContext.request.contextPath}/employees/leaveList">Urlopy</a>
            </li>
            <li>
                <a href="${pageContext.request.contextPath}/employees/wholeHistoryForUser">Pokaż historię pracy</a>
            </li>
        </ul>

        <ul class="list-unstyled CTAs">
            <li><form:form action="${pageContext.request.contextPath}/logout" method="POST">
                <input type="submit" value="Wyloguj się" class="btn btn-danger btn-block">
            </form:form>
            </li>
        </ul>
    </nav>
    <div id="content">

        <nav id="navbar" class="navbar navbar-default">
            <div class="content-fluid">

                <div class="navbar-header">
                    <button type="button" id="sidebarCollapse" class="btn btn-info navbar-btn">
                        <i class="glyphicon glyphicon-align-left"></i>
                    </button>
                </div>

                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right">
                        <li><form:form action="${pageContext.request.contextPath}/logout" method="POST">
                            <button type="submit" class="btn btn-danger btn-block">
                                <span class="glyphicon glyphicon-off"></span> Wyloguj się
                            </button>
                        </form:form></li>
                    </ul>
                </div>
            </div>
        </nav>
        <div class="container">
            <h3>Twoje podania o urlop</h3>
            <hr>
            <c:choose>
                <c:when test="${leaves.size()==0}">
                    <h4>Brak podań o urlop!</h4>
                </c:when>
                <c:otherwise>
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>Nr</th>
                            <th>Początek urlopu</th>
                            <th>Koniec urlopu</th>
                            <th>Status</th>
                            <th>Operacja</th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach var="tempLeave" items="${leaves}" varStatus="i">
                            <c:choose>
                                <c:when test="${tempLeave.stateByStateId.stateId==6}">
                                    <tr class="info">
                                </c:when>
                                <c:when test="${tempLeave.stateByStateId.stateId==7}">
                                    <tr class="danger">
                                </c:when>
                                <c:when test="${tempLeave.stateByStateId.stateId==8}">
                                    <tr class="success">
                                </c:when>
                            </c:choose>
                                <td>${offset + i.index + 1}</td>
                                <td>${tempLeave.dateFrom}</td>
                                <td>${tempLeave.dateTo}</td>
                                <td>${tempLeave.stateByStateId.state}</td>
                                <td>
                                    <c:url var="deleteLink" value="/employees/deleteLeave">
                                        <c:param name="leaveId" value="${tempLeave.leaveId}"></c:param>
                                    </c:url>
                                    <a href="${deleteLink}" class="btn btn-danger"
                                       onclick="if (!(confirm('Czy na pewno chcesz usunać podanie o urlop?'))) return false">
                                        <span class="glyphicon glyphicon-remove"></span>
                                        Usuń
                                    </a>
                                </td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                    <tag:paginate max="15" offset="${offset}" count="${count}"
                                  uri="/employees/leaveList" next="&raquo;" previous="&laquo;" />
                </c:otherwise>
            </c:choose>

            <h3>Wpisz urlop</h3>
            <hr>
            <form:form id="newLeave" action="${pageContext.request.contextPath}/employees/saveLeave" modelAttribute="leave" method="POST">
                <form:hidden id="dateFrom" path="dateFrom"></form:hidden>
                <form:hidden id="dateTo" path="dateTo"></form:hidden>
                <form:hidden id="stateId" path="stateByStateId.stateId"></form:hidden>
            </form:form>
            <div id='calendar'></div>
            <hr>

            <hr>
            <button onclick="window.location.href='/employees'" type="button" class="btn btn-primary">Wstecz</button>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {

        $("#calendar").fullCalendar({
            selectable: true,
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,agendaWeek,agendaDay',
            },
            navLinks: true,
            eventLimit: true,
            defaultView: 'month',
            showNonCurrentDates: true,
            fixedWeekCount: false,
            locale: 'pl',
            height: 500,
            weekends: false,
            defaultTimedEventDuration: 1,
            displayEventTime: false,
            displayEventEnd: true,
            events: 'http://localhost:8080/leavesJsonForAllUsers',
            eventRender: function (event, element) {
                if(event.status == '6'){
                    element.css('background-color', 'blue');
                    element.find('.fc-title').append("Urlop zgłoszony: " + event.userName);
                }
                else if(event.status == '7'){
                    element.css('background-color', 'red');
                    element.find('.fc-title').append("Urlop odrzucony: " + event.userName);
                }
                else{
                    element.css('background-color', 'green');
                    element.find('.fc-title').append("Urlop: " + event.userName);
                }
            },
            // dodawanie urlopu
            select: function(startDate, endDate) {
                var currentDate = moment().format('YYYY/MM/DD');
                // sprawdzanie czy manager wybrał datę późniejszą niż obecna
                if( currentDate > startDate.format('YYYY/MM/DD')){
                    alert('Wybierz późniejszą datę!')
                } else {
                    var tempDate = new Date(endDate);
                    if (tempDate.getMonth() + 1 == 1 && tempDate.getUTCDate() == 1) {
                        // sprawdz czy podane daty są dłuższe niż 20 dni
                        if (getBusinessDatesCount(new Date(startDate), new Date(getOneDayBefore(endDate))) > 20) {
                            alert('Maksymalny czas urlopu 20 dni w roku');
                        } else {
                            var year = new Date(startDate).getFullYear();
                            $.ajax({
                                url: 'http://localhost:8080/employees/getLeaveForUser',
                                type: "GET",
                                dateType: 'json',
                                data: {'year': year},
                                success: function (data) {
                                    var listOfUserLeaves = $.parseJSON(data);
                                    var freeDaysForLeave = true;
                                    // sprawdzenie czy podany okres urlopu nie zawiera się w zapisanym w bazie (jeden użytkownik nie może wziąć urlopu w tym sammym czasie)
                                    for (i = 0; i < listOfUserLeaves.length; i++) {
                                        var startDateFromDB = new Date(listOfUserLeaves[i].start);
                                        var endDateFromDB = new Date(listOfUserLeaves[i].end);
                                        if (startDate < startDateFromDB && endDate <= endDateFromDB && endDate > startDateFromDB)
                                            freeDaysForLeave = false;
                                        else if (startDate >= startDateFromDB && endDate > endDateFromDB && startDate < endDateFromDB)
                                            freeDaysForLeave = false;
                                        else if (startDate >= startDateFromDB && endDate <= endDateFromDB)
                                            freeDaysForLeave = false;
                                        else if (startDate < startDateFromDB && endDate > endDateFromDB)
                                            freeDaysForLeave = false;
                                    }
                                    if (freeDaysForLeave) {
                                        // liczenie dni wykorzystanycg na urlop (tylko pracujące, max 20 dni)
                                        var numberOfLeaveDays = 0;
                                        for (i = 0; i < listOfUserLeaves.length; i++) {
                                            var startDateFromDB = new Date(listOfUserLeaves[i].start);
                                            var endDateFromDB = new Date(listOfUserLeaves[i].end);
                                            numberOfLeaveDays += getBusinessDatesCount(startDateFromDB, new Date(getOneDayBefore(endDateFromDB)));
                                        }
                                        var numberOfUsedDays = numberOfLeaveDays;
                                        numberOfLeaveDays += getBusinessDatesCount(new Date(startDate), new Date(getOneDayBefore(endDate)));
                                        // sprawdzanie czy użytkownik wykorzystał swój urlop
                                        if (numberOfLeaveDays > 20) {
                                            alert('Limit na urlop przekroczony!\nWykorzystane dni: ' + numberOfUsedDays + '\nWykorzystane dni z nowym urlopem: ' + numberOfLeaveDays);
                                        } else {
                                            var r = confirm('Czy na pewno chcesz dodać urlop?');
                                            if (r === true) {
                                                document.getElementById("dateFrom").value = startDate.format('YYYY/MM/DD');
                                                if (new Date(endDate).getTime() - new Date(startDate).getTime() > 24 * 60 * 60 * 1000)
                                                    document.getElementById("dateTo").value = endDate.format('YYYY/MM/DD');
                                                else
                                                    document.getElementById("dateTo").value = getOneDayBefore(endDate);
//                                                $('#stateId').val(8);
                                                document.getElementById("stateId").value = 8;
                                                document.getElementById("newLeave").submit();
                                                alert('Urlop dodany!\nWykorzystane dni przed dodaniem: ' + numberOfUsedDays + '\nWykorzystane dni po dodaniu: ' + numberOfLeaveDays);
                                            }
                                        }
                                    }
                                    else
                                        alert('Posiadasz już urlop w tym czasie!');
                                }
                            });
                        }
                    } else {
                        // sprawdz czy daty są w różnych latach
                        if ((new Date(startDate).getYear()) != (new Date(endDate).getYear())) {
                            alert('Termin musi zawierać się w tym samym roku!');
                        } else {
                            // sprawdz czy podane daty są dłuższe niż 20 dni
                            if (getBusinessDatesCount(new Date(startDate), new Date(getOneDayBefore(endDate))) > 20) {
                                alert('Maksymalny czas urlopu 20 dni w roku');
                            } else {
                                var year = new Date(startDate).getFullYear();
                                $.ajax({
                                    url: 'http://localhost:8080/employees/getLeaveForUser',
                                    type: "GET",
                                    dateType: 'json',
                                    data: {'year': year},
                                    success: function (data) {
                                        var listOfUserLeaves = $.parseJSON(data);
                                        var freeDaysForLeave = true;
                                        // sprawdzenie czy podany okres urlopu nie zawiera się w zapisanym w bazie (jeden użytkownik nie może wziąć urlopu w tym sammym czasie)
                                        for (i = 0; i < listOfUserLeaves.length; i++) {
                                            var startDateFromDB = new Date(listOfUserLeaves[i].start);
                                            var endDateFromDB = new Date(listOfUserLeaves[i].end);
                                            if (startDate < startDateFromDB && endDate <= endDateFromDB && endDate > startDateFromDB)
                                                freeDaysForLeave = false;
                                            else if (startDate >= startDateFromDB && endDate > endDateFromDB && startDate < endDateFromDB)
                                                freeDaysForLeave = false;
                                            else if (startDate >= startDateFromDB && endDate <= endDateFromDB)
                                                freeDaysForLeave = false;
                                            else if (startDate < startDateFromDB && endDate > endDateFromDB)
                                                freeDaysForLeave = false;
                                        }
                                        if (freeDaysForLeave) {
                                            // liczenie dni wykorzystanycg na urlop (tylko pracujące, max 20 dni)
                                            var numberOfLeaveDays = 0;
                                            for (i = 0; i < listOfUserLeaves.length; i++) {
                                                var startDateFromDB = new Date(listOfUserLeaves[i].start);
                                                var endDateFromDB = new Date(listOfUserLeaves[i].end);
                                                numberOfLeaveDays += getBusinessDatesCount(startDateFromDB, new Date(getOneDayBefore(endDateFromDB)));
                                            }
                                            var numberOfUsedDays = numberOfLeaveDays;
                                            numberOfLeaveDays += getBusinessDatesCount(new Date(startDate), new Date(getOneDayBefore(endDate)));
                                            // sprawdzanie czy użytkownik wykorzystał swój urlop
                                            if (numberOfLeaveDays > 20) {
                                                alert('Limit na urlop przekroczony!\nWykorzystane dni: ' + numberOfUsedDays + '\nWykorzystane dni z nowym urlopem: ' + numberOfLeaveDays);
                                            } else {
                                                var r = confirm('Czy na pewno chcesz dodać urlop?');
                                                if (r === true) {
                                                    document.getElementById("dateFrom").value = startDate.format('YYYY/MM/DD');
                                                    if (new Date(endDate).getTime() - new Date(startDate).getTime() > 24 * 60 * 60 * 1000)
                                                        document.getElementById("dateTo").value = endDate.format('YYYY/MM/DD');
                                                    else
                                                        document.getElementById("dateTo").value = getOneDayBefore(endDate);
                                                    document.getElementById("stateId").value = 6;
                                                    document.getElementById("newLeave").submit();
                                                    alert('Urlop dodany!\nWykorzystane dni przed dodaniem: ' + numberOfUsedDays + '\nWykorzystane dni po dodaniu: ' + numberOfLeaveDays);
                                                }
                                            }
                                        }
                                        else
                                            alert('Posiadasz już urlop w tym czasie!');
                                    }
                                });
                            }
                        }
                    }
                }
            }

    });
    });
    function getBusinessDatesCount(startDate, endDate) {
        var count = 0;
        var curDate = startDate;
        while (curDate <= endDate) {
            var dayOfWeek = curDate.getDay();
            if(!((dayOfWeek === 6) || (dayOfWeek === 0)))
                count++;
            curDate.setDate(curDate.getDate() + 1);
        }
        return count+1;
    }
    function getOneDayBefore(date) {
        var newDate = new Date(date);
        newDate.setTime(newDate.getTime() - (24*60*60*1000));
        var date1 = new Date(newDate);
        year1 = date1.getFullYear();
        month1 = date1.getMonth()+1;
        day1 = date1.getUTCDate();
        day = year1+'/'+month1+'/'+day1;
        return day;
    }
</script>
</body>
</html>
