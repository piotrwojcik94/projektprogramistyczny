$(document).ready(function() {

    $("#calendar").fullCalendar({
        selectable: true,
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay',
        },
        navLinks: true,
        eventLimit: true,
        defaultView: 'month',
        showNonCurrentDates: true,
        fixedWeekCount: false,
        locale: 'pl',
        height: 500,
        weekends: false,
        defaultTimedEventDuration: 1,
        displayEventTime: false,
        displayEventEnd: true,
        events: 'http://localhost:8080/leavesJsonForAllUsers',
        eventRender: function (event, element) {
            if(event.status == '6'){
                element.css('background-color', 'blue');
                element.find('.fc-title').append("Urlop zgłoszony: " + event.userName);
            }
            else if(event.status == '7'){
                element.css('background-color', 'red');
                element.find('.fc-title').append("Urlop odrzucony: " + event.userName);
            }
            else{
                element.css('background-color', 'green');
                element.find('.fc-title').append("Urlop: " + event.userName);
            }
        },
        // dodawanie urlopu
        select: function(startDate, endDate) {
            var currentDate = moment().format('YYYY/MM/DD');
            // sprawdzanie czy manager wybrał datę późniejszą niż obecna
            if( currentDate > startDate.format('YYYY/MM/DD')){
                alert('Wybierz późniejszą datę!')
            } else {
                var tempDate = new Date(endDate);
                if ((tempDate.getMonth() + 1 === 1) && (tempDate.getUTCDate() === 1)) {
                    // sprawdz czy podane daty są dłuższe niż 20 dni
                    if (getBusinessDatesCount(new Date(startDate), new Date(getOneDayBefore(endDate))) > 20) {
                        alert('Maksymalny czas urlopu 20 dni w roku');
                    } else {
                        var year = new Date(startDate).getFullYear();
                        $.ajax({
                            url: 'http://localhost:8080/managers/getLeaveForUser',
                            type: "GET",
                            dateType: 'json',
                            data: {'year': year},
                            success: function (data) {
                                var listOfUserLeaves = $.parseJSON(data);
                                var freeDaysForLeave = true;
                                // sprawdzenie czy podany okres urlopu nie zawiera się w zapisanym w bazie (jeden użytkownik nie może wziąć urlopu w tym sammym czasie)
                                for (i = 0; i < listOfUserLeaves.length; i++) {
                                    var startDateFromDB = new Date(listOfUserLeaves[i].start);
                                    var endDateFromDB = new Date(listOfUserLeaves[i].end);
                                    if (startDate < startDateFromDB && endDate <= endDateFromDB && endDate > startDateFromDB)
                                        freeDaysForLeave = false;
                                    else if (startDate >= startDateFromDB && endDate > endDateFromDB && startDate < endDateFromDB)
                                        freeDaysForLeave = false;
                                    else if (startDate >= startDateFromDB && endDate <= endDateFromDB)
                                        freeDaysForLeave = false;
                                    else if (startDate < startDateFromDB && endDate > endDateFromDB)
                                        freeDaysForLeave = false;
                                }
                                if (freeDaysForLeave) {
                                    // liczenie dni wykorzystanycg na urlop (tylko pracujące, max 20 dni)
                                    var numberOfLeaveDays = 0;
                                    for (i = 0; i < listOfUserLeaves.length; i++) {
                                        var startDateFromDB = new Date(listOfUserLeaves[i].start);
                                        var endDateFromDB = new Date(listOfUserLeaves[i].end);
                                        numberOfLeaveDays += getBusinessDatesCount(startDateFromDB, new Date(getOneDayBefore(endDateFromDB)));
                                    }
                                    var numberOfUsedDays = numberOfLeaveDays;
                                    numberOfLeaveDays += getBusinessDatesCount(new Date(startDate), new Date(getOneDayBefore(endDate)));
                                    // sprawdzanie czy użytkownik wykorzystał swój urlop
                                    if (numberOfLeaveDays > 20) {
                                        alert('Limit na urlop przekroczony!\nWykorzystane dni: ' + numberOfUsedDays + '\nWykorzystane dni z nowym urlopem: ' + numberOfLeaveDays);
                                    } else {
                                        var r = confirm('Czy na pewno chcesz dodać urlop?');
                                        if (r === true) {
                                            document.getElementById("dateFrom").value = startDate.format('YYYY/MM/DD');
                                            if (new Date(endDate).getTime() - new Date(startDate).getTime() > 24 * 60 * 60 * 1000)
                                                document.getElementById("dateTo").value = endDate.format('YYYY/MM/DD');
                                            else
                                                document.getElementById("dateTo").value = getOneDayBefore(endDate);
                                            document.getElementById("stateId").value = 8;
                                            document.getElementById("newLeave").submit();
                                            alert('Urlop dodany!\nWykorzystane dni przed dodaniem: ' + numberOfUsedDays + '\nWykorzystane dni po dodaniu: ' + numberOfLeaveDays);
                                        }
                                    }
                                }
                                else
                                    alert('Posiadasz już urlop w tym czasie!');
                            }
                        });
                    }
                } else {
                    // sprawdz czy daty są w różnych latach
                    if (((new Date(startDate).getYear())) !== ((new Date(endDate).getYear()))) {
                        alert('Termin musi zawierać się w tym samym roku!');
                    } else {
                        // sprawdz czy podane daty są dłuższe niż 20 dni
                        if (getBusinessDatesCount(new Date(startDate), new Date(getOneDayBefore(endDate))) > 20) {
                            alert('Maksymalny czas urlopu 20 dni w roku');
                        } else {
                            var year = new Date(startDate).getFullYear();
                            $.ajax({
                                url: 'http://localhost:8080/managers/getLeaveForUser',
                                type: "GET",
                                dataType: 'json',
                                data: {'year': year},
                                success: function (data) {
                                    var listOfUserLeaves = $.parseJSON(JSON.stringify(data));
                                    var freeDaysForLeave = true;
                                    // sprawdzenie czy podany okres urlopu nie zawiera się w zapisanym w bazie (jeden użytkownik nie może wziąć urlopu w tym sammym czasie)
                                    for (i = 0; i < listOfUserLeaves.length; i++) {
                                        var startDateFromDB = new Date(listOfUserLeaves[i].start);
                                        var endDateFromDB = new Date(listOfUserLeaves[i].end);
                                        if (startDate < startDateFromDB && endDate <= endDateFromDB && endDate > startDateFromDB)
                                            freeDaysForLeave = false;
                                        else if (startDate >= startDateFromDB && endDate > endDateFromDB && startDate < endDateFromDB)
                                            freeDaysForLeave = false;
                                        else if (startDate >= startDateFromDB && endDate <= endDateFromDB)
                                            freeDaysForLeave = false;
                                        else if (startDate < startDateFromDB && endDate > endDateFromDB)
                                            freeDaysForLeave = false;
                                    }
                                    if (freeDaysForLeave) {
                                        // liczenie dni wykorzystanycg na urlop (tylko pracujące, max 20 dni)
                                        var numberOfLeaveDays = 0;
                                        for (i = 0; i < listOfUserLeaves.length; i++) {
                                            var startDateFromDB = new Date(listOfUserLeaves[i].start);
                                            var endDateFromDB = new Date(listOfUserLeaves[i].end);
                                            numberOfLeaveDays += getBusinessDatesCount(startDateFromDB, new Date(getOneDayBefore(endDateFromDB)));
                                        }
                                        var numberOfUsedDays = numberOfLeaveDays;
                                        numberOfLeaveDays += getBusinessDatesCount(new Date(startDate), new Date(getOneDayBefore(endDate)));
                                        // sprawdzanie czy użytkownik wykorzystał swój urlop
                                        if (numberOfLeaveDays > 20) {
                                            alert('Limit na urlop przekroczony!\nWykorzystane dni: ' + numberOfUsedDays + '\nWykorzystane dni z nowym urlopem: ' + numberOfLeaveDays);
                                        } else {
                                            var r = confirm('Czy na pewno chcesz dodać urlop?');
                                            if (r === true) {
                                                document.getElementById("dateFrom").value = startDate.format('YYYY/MM/DD');
                                                if (new Date(endDate).getTime() - new Date(startDate).getTime() > 24 * 60 * 60 * 1000)
                                                    document.getElementById("dateTo").value = endDate.format('YYYY/MM/DD');
                                                else
                                                    document.getElementById("dateTo").value = getOneDayBefore(endDate);
                                                document.getElementById("stateId").value = 8;
                                                document.getElementById("newLeave").submit();
                                                alert('Urlop dodany!\nWykorzystane dni przed dodaniem: ' + numberOfUsedDays + '\nWykorzystane dni po dodaniu: ' + numberOfLeaveDays);
                                            }
                                        }
                                    }
                                    else
                                        alert('Posiadasz już urlop w tym czasie!');
                                }
                            });
                        }
                    }
                }
            }
        }
    });

});
function getBusinessDatesCount(startDate, endDate) {
    var count = 0;
    var curDate = startDate;
    while (curDate <= endDate) {
        var dayOfWeek = curDate.getDay();
        if(!((dayOfWeek === 6) || (dayOfWeek === 0)))
            count++;
        curDate.setDate(curDate.getDate() + 1);
    }
    return count+1;
}
function getOneDayBefore(date) {
    var newDate = new Date(date);
    newDate.setTime(newDate.getTime() - (24*60*60*1000));
    var date1 = new Date(newDate);
    year1 = date1.getFullYear();
    month1 = date1.getMonth()+1;
    day1 = date1.getUTCDate();
    day = year1+'/'+month1+'/'+day1;
    return day;
}