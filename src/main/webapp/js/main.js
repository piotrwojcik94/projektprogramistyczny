var x=0;
$(document).ready(function () {
    $('#sidebarCollapse').on('click', function () {
        $('#sidebar').toggleClass('active');
        var navbar = document.getElementById("navbar");
        var content = document.getElementById("content");
        if(x%2==0){
            navbar.classList.remove("sticky");
            navbar.classList.add("sticky2");
            content.style.width = "99.6%" ;
            content.style.paddingTop = "90px";
            x=x+1;
        } else {
            navbar.classList.remove("sticky2");
            navbar.classList.add("sticky");
            content.style.paddingTop = "0";
            x=x+1;
        }
    });

});
window.onscroll = function() {myFunction()};

var navbar = document.getElementById("navbar");
var sticky = navbar.offsetTop;

function myFunction() {
    if (window.pageYOffset >= sticky) {
        navbar.classList.add("sticky");
    } else {
        navbar.classList.remove("sticky");
    }
}
