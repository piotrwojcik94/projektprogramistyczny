<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<html>
<head>
    <title>Brak dostępu</title>
</head>
<body>
    <h1>Brak dostępu do strony!</h1>
    <hr>
    <h2><a href="/">Powrót</a></h2>
</body>
</html>
