<%--
  Created by IntelliJ IDEA.
  User: Piotr
  Date: 19.03.2018
  Time: 19:56
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="tag" uri="/WEB-INF/taglibs/customTaglib.tld"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<html>
<head>
    <title>Panel administratora</title>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.css" rel="stylesheet"/>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js" ></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.js" type="text/javascript" ></script>
    <link href="/css/main.css" rel="stylesheet" media="screen">
    <link href="/css/settings.css" rel="stylesheet" media="screen">
    <script src="/js/main.js" type="text/javascript" media="screen"></script>

</head>
<body>
<div class="wrapper">
    <nav id="sidebar">
        <div class="sidebar-header">
            <h4>Zalogowany jako: <security:authentication property="principal.username"/></h4>
            <div class="dropdown">
                <div class="setting">
                    <h4 style="display: inline;"><span class="glyphicon glyphicon-user"></span></h4>
                    Ustawienia
                </div>
                <div class="dropdown-content">
                    <a href="${pageContext.request.contextPath}/admin/changeLogin">Zmień login</a>
                    <a href="${pageContext.request.contextPath}/admin/changePassword">Zmień hasło</a>
                </div>
            </div>
        </div>

        <ul class="list-unstyled components">
            <p><b>Opcje użytkownika</b></p>
            <li>
                <a href="${pageContext.request.contextPath}/admin/showFormForAdd">Dodaj użytkownika</a>
            </li>
            <li>
                <a href="${pageContext.request.contextPath}/admin/showFormForAddCard">Dodaj kartę dostępu</a>
            </li>
            <li>
                <a href="${pageContext.request.contextPath}/admin/formForAddHistory">Nasłuchuj czytnik kart</a>
            </li>
        </ul>

        <ul class="list-unstyled CTAs">
            <li><form:form action="${pageContext.request.contextPath}/logout" method="POST">
                <input type="submit" value="Wyloguj się" class="btn btn-danger btn-block">
            </form:form>
            </li>
        </ul>
    </nav>
    <div id="content">

        <nav id="navbar" class="navbar navbar-default">
            <div class="content-fluid">

                <div class="navbar-header">
                    <button type="button" id="sidebarCollapse" class="btn btn-info navbar-btn">
                        <i class="glyphicon glyphicon-align-left"></i>
                    </button>
                </div>

                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right">
                        <li><form:form action="${pageContext.request.contextPath}/logout" method="POST">
                            <button type="submit" class="btn btn-danger btn-block">
                                <span class="glyphicon glyphicon-off"></span> Wyloguj się
                            </button>
                        </form:form></li>
                    </ul>
                </div>
            </div>
        </nav>
        <div class="container">
            <h2>Użytkownicy systemu</h2>
            <hr>
            <c:choose>
                <c:when test="${users.size()==0}">
                    <h3>Brak użytkowników w systemie</h3>
                </c:when>
                <c:otherwise>
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>Nr</th>
                            <th>Imię</th>
                            <th>Nazwisko</th>
                            <th>Login</th>
                            <th>Wiek</th>
                            <th>Funkcja</th>
                            <th>Więcej</th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach var="tempUser" items="${users}" varStatus="i">
                            <tr>
                                <td>${offset + i.index + 1}</td>
                                <td>${tempUser.firstName}</td>
                                <td>${tempUser.lastName}</td>
                                <td>${tempUser.accountLogin}</td>
                                <td>${tempUser.age}</td>
                                <td>${tempUser.roleByRoleId.role}</td>
                                <td>
                                    <c:url var="updateLink" value="/admin/showFormForUpdate">
                                        <c:param name="userId" value="${tempUser.userId}"></c:param>
                                    </c:url>
                                    <button onclick="window.location.href='${updateLink}'" type="button" class="btn btn-warning">
                                        Zmień dane
                                    </button>
                                    <c:url var="resetLink" value="/admin/resetUserPassword">
                                        <c:param name="userId" value="${tempUser.userId}"></c:param>
                                    </c:url>
                                    <a href="${resetLink}" class="btn btn-primary"
                                       onclick="if (!(confirm('Czy na pewno chcesz zresetować hasło\nużytkownika ${tempUser.firstName} ${tempUser.lastName}?'))) return false">
                                        Resetuj
                                    </a>
                                    <c:url var="deleteLink" value="/admin/deleteUser">
                                        <c:param name="userId" value="${tempUser.userId}"></c:param>
                                    </c:url>
                                    <a href="${deleteLink}" class="btn btn-danger"
                                       onclick="if (!(confirm('Czy na pewno chcesz usunąć usera?'))) return false">
                                        <span class="glyphicon glyphicon-remove"></span>
                                        Usuń
                                    </a>
                                </td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                    <tag:paginate max="15" offset="${offset}" count="${count}"
                                  uri="/admin" next="&raquo;" previous="&laquo;" />
                </c:otherwise>
            </c:choose>
        </div>
    </div>
</div>
</body>
</html>