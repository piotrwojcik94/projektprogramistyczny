<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>

<head>
    <title>Logowanie do systemu</title>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.css" rel="stylesheet"/>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.css" rel="stylesheet"/>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js" ></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.js" type="text/javascript" ></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
    <meta charset="UTF-8">
</head>

<body>
<div class="container">
    <div class="col-lg-4"></div>
    <div id="loginbox" style="margin-top: 50px;"
         class="col-lg-4">
        <div class="panel panel-info">
            <div class="panel-heading">
                <div class="panel-title">Zaloguj się do systemu</div>
            </div>
            <div style="padding-top: 30px" class="panel-body">
                <form:form action="${pageContext.request.contextPath}/loginAction"
                           method="POST" class="form-horizontal">
                    <div class="form-group">
                        <div class="col-xs-15">
                            <div>
                                <c:choose>
                                    <c:when test="${sessionScope.SPRING_SECURITY_LAST_EXCEPTION.message == 'Maximum sessions of 1 for this principal exceeded'}">
                                        <div class="alert alert-danger col-xs-offset-1 col-xs-10">
                                            Użytkownik aktualnie zalogowany!
                                        </div>
                                    </c:when>
                                    <c:otherwise>
                                        <c:if test="${param.error != null}">
                                            <div class="alert alert-danger col-xs-offset-1 col-xs-10">
                                                Zły login lub hasło!
                                            </div>
                                        </c:if>
                                    </c:otherwise>
                                </c:choose>
                                <c:if test="${param.logout != null}">
                                    <div class="alert alert-success col-xs-offset-1 col-xs-10">
                                        Wylogowany pomyślnie!
                                    </div>
                                </c:if>
                            </div>
                        </div>
                    </div>
                    <div style="margin-bottom: 25px" class="input-group">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                        <input type="text" name="username" placeholder="login" class="form-control" autofocus>
                    </div>
                    <div style="margin-bottom: 25px" class="input-group">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>

                        <input type="password" name="password" placeholder="hasło" class="form-control" >
                    </div>
                    <div style="margin-top: 10px" class="form-group">
                        <div class="col-sm-6 controls">
                            <button type="submit" class="btn btn-success">Zaloguj się!</button>
                        </div>
                    </div>
                </form:form>
            </div>
        </div>
    </div>
</div>
</body>

</html>
