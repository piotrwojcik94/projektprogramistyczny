<%--
  Created by IntelliJ IDEA.
  User: Piotr
  Date: 25.03.2018
  Time: 20:29
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<html>
<head>
    <title>Dodaj kartę dostępu</title>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.css" rel="stylesheet"/>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js" ></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.js" type="text/javascript"></script>
    <link href="/css/main.css" rel="stylesheet" media="screen">
    <link href="/css/settings.css" rel="stylesheet" media="screen">
    <script src="/js/main.js" type="text/javascript" media="screen"></script>
</head>
<body>
<div class="wrapper">
    <nav id="sidebar">
        <div class="sidebar-header">
            <h4>Zalogowany jako: <security:authentication property="principal.username"/></h4>
            <div class="dropdown">
                <div class="setting">
                    <h4 style="display: inline;"><span class="glyphicon glyphicon-user"></span></h4>
                    Ustawienia
                </div>
                <div class="dropdown-content">
                    <a href="${pageContext.request.contextPath}/admin/changeLogin">Zmień login</a>
                    <a href="${pageContext.request.contextPath}/admin/changePassword">Zmień hasło</a>
                </div>
            </div>
        </div>

        <ul class="list-unstyled components">
            <p><b>Opcje użytkownika</b></p>
            <li>
                <a href="${pageContext.request.contextPath}/admin/showFormForAdd">Dodaj użytkownika</a>
            </li>
            <li>
                <a href="${pageContext.request.contextPath}/admin/showFormForAddCard">Dodaj kartę dostępu</a>
            </li>
            <li>
                <a href="${pageContext.request.contextPath}/admin/formForAddHistory">Nasłuchuj czytnik kart</a>
            </li>
        </ul>

        <ul class="list-unstyled CTAs">
            <li><form:form action="${pageContext.request.contextPath}/logout" method="POST">
                <input type="submit" value="Wyloguj się" class="btn btn-danger btn-block">
            </form:form>
            </li>
        </ul>
    </nav>
    <div id="content">

        <nav id="navbar" class="navbar navbar-default">
            <div class="content-fluid">

                <div class="navbar-header">
                    <button type="button" id="sidebarCollapse" class="btn btn-info navbar-btn">
                        <i class="glyphicon glyphicon-align-left"></i>
                    </button>
                </div>

                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right">
                        <li><form:form action="${pageContext.request.contextPath}/logout" method="POST">
                            <button type="submit" class="btn btn-danger btn-block">
                                <span class="glyphicon glyphicon-off"></span> Wyloguj się
                            </button>
                        </form:form></li>
                    </ul>
                </div>
            </div>
        </nav>
        <div class="container">
            <h2>Dodaj nową kartę</h2>
            <hr>
            <form:form id="newCardForm" action="/admin/saveCard" modelAttribute="card" method="POST">
                <div class="form-group" style="padding-bottom: 40px;">
                    <label class="control-label col-sm-6" ><input type="checkbox" id="newCard"> Istniejąca karta</label>
                    <div class="col-sm-6"></div>
                </div>
                <div id="cardIdSelect" class="form-group" style="padding-bottom: 40px;display: none;">
                    <label class="control-label col-sm-2" for="cardId1">Id karty:</label>
                    <div class="col-sm-4">
                        <form:select path="" class="form-control" id="cardId1" name="cardId1">
                            <form:option value="0" selected="selected" >&nbsp</form:option>
                            <c:forEach var="tempCard" items="${cardsWithoutUsers}">
                                <form:option value="${tempCard.cardId}">
                                    ${tempCard.cardId}
                                </form:option>
                            </c:forEach>
                        </form:select>
                        <form:errors path="cardId" cssClass="error" />
                    </div>
                    <div class="col-sm-6"></div>
                </div>
                <div id="cardIdInput" class="form-group" style="padding-bottom: 40px;">
                    <label class="control-label col-sm-2" for="cardId">Id karty:</label>
                    <div class="col-sm-4">
                        <form:input path="cardId" type="text" autofocus="autofocus" class="form-control" id="cardId" placeholder="Wprowadź numer karty" name="cardId" onfocus="this.value=''"/>
                        <form:errors path="cardId" cssClass="error" />
                    </div>
                    <div class="col-sm-6"></div>
                </div>
                <div style="clear: both;"></div>
                <div class="form-group" style="padding-bottom: 40px;">
                    <label class="control-label col-sm-2" for="userId">Użytkownik:</label>
                    <div class="col-sm-4">
                        <form:select path="userByUserId.userId" class="form-control" id="userId" name="userId">
                            <form:option value="0" selected="selected" >&nbsp</form:option>
                            <c:forEach var="tempUser" items="${usersWithoutCard}">
                                <form:option value="${tempUser.userId}">
                                    ${tempUser.firstName} ${tempUser.lastName}
                                </form:option>
                            </c:forEach>
                        </form:select>
                    </div>
                    <div class="col-sm-10"></div>
                </div>
                <div style="clear: both;"></div>
                <div class="form-group">
                    <div class="control-label col-sm-2"></div>
                    <div class="col-sm-4">
                        <a class="btn btn-success btn-block"
                           onclick="addNewCard()">
                            Dodaj kartę
                        </a>
                    </div>
                    <div class="col-sm-10"></div>
                </div>
                <div style="clear: both;"></div>
            </form:form>
            <hr>
            <button onclick="window.location.href='/'" type="button" class="btn btn-primary">Wstecz</button>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('#newCard').change(function () {
            if(this.checked){
                $('#cardIdSelect').fadeIn('fast');
                $('#cardIdInput').fadeOut('slow');
            } else {
                $('#cardIdSelect').fadeOut('slow');
                $('#cardIdInput').fadeIn('fast');
            }
        });
    });
    function addNewCard() {
        if($('#newCard').prop('checked')){
            // zaznaczone
            document.getElementById("cardId").value = document.getElementById("cardId1").value;
            document.getElementById("newCardForm").submit();
        } else {
            // niezaznaczone
            document.getElementById("newCardForm").submit();
        }
    }
</script>
</body>
</html>