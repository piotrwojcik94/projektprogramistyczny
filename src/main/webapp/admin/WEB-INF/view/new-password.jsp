<%--
  Created by IntelliJ IDEA.
  User: Piotr
  Date: 02.05.2018
  Time: 11:41
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="tag" uri="/WEB-INF/taglibs/customTaglib.tld"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<html>
<head>
    <title>Zmień hasło</title>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.css" rel="stylesheet"/>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js" ></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.js" type="text/javascript" ></script>
    <link href="/css/main.css" rel="stylesheet" media="screen">
    <link href="/css/settings.css" rel="stylesheet" media="screen">
    <script src="/js/main.js" type="text/javascript" media="screen"></script>

</head>
<body>
<div class="wrapper">
    <nav id="sidebar">
        <div class="sidebar-header">
            <h4>Zalogowany jako: <security:authentication property="principal.username"/></h4>
            <div class="dropdown">
                <div class="setting">
                    <h4 style="display: inline;"><span class="glyphicon glyphicon-user"></span></h4>
                    Ustawienia
                </div>
                <div class="dropdown-content">
                    <a href="${pageContext.request.contextPath}/admin/changeLogin">Zmień login</a>
                    <a href="${pageContext.request.contextPath}/admin/changePassword">Zmień hasło</a>
                </div>
            </div>
        </div>

        <ul class="list-unstyled components">
            <p><b>Opcje użytkownika</b></p>
            <li>
                <a href="${pageContext.request.contextPath}/admin/showFormForAdd">Dodaj użytkownika</a>
            </li>
            <li>
                <a href="${pageContext.request.contextPath}/admin/showFormForAddCard">Dodaj kartę dostępu</a>
            </li>
            <li>
                <a href="${pageContext.request.contextPath}/admin/formForAddHistory">Nasłuchuj czytnik kart</a>
            </li>
        </ul>

        <ul class="list-unstyled CTAs">
            <li><form:form action="${pageContext.request.contextPath}/logout" method="POST">
                <input type="submit" value="Wyloguj się" class="btn btn-danger btn-block">
            </form:form>
            </li>
        </ul>
    </nav>
    <div id="content">

        <nav id="navbar" class="navbar navbar-default">
            <div class="content-fluid">

                <div class="navbar-header">
                    <button type="button" id="sidebarCollapse" class="btn btn-info navbar-btn">
                        <i class="glyphicon glyphicon-align-left"></i>
                    </button>
                </div>

                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right">
                        <li><form:form action="${pageContext.request.contextPath}/logout" method="POST">
                            <button type="submit" class="btn btn-danger btn-block">
                                <span class="glyphicon glyphicon-off"></span> Wyloguj się
                            </button>
                        </form:form></li>
                    </ul>
                </div>
            </div>
        </nav>
        <div class="container">
            <h2>Zmień hasło</h2>
            <hr>
            <form:form id="newUserForm" action="/admin/saveNewPassword" modelAttribute="user" method="POST">

                <form:hidden path="userId"/>
                <form:hidden path="firstName"/>
                <form:hidden path="lastName"/>
                <form:hidden path="age"/>
                <form:hidden path="accountLogin"/>
                <form:hidden path="roleByRoleId.roleId"/>
                <div class="form-group" style="padding-bottom: 40px;">
                    <label class="control-label col-sm-2" for="accountPassword">Nowe hasło:</label>
                    <div class="col-sm-4">
                        <form:input path="accountPassword" type="password" class="form-control" autofocus="autofocus" placeholder="Wprowadz nowe hasło"/>
                    </div>
                    <div class="col-sm-6"></div>
                </div>
                <div style="clear: both;"></div>
                <div class="form-group">
                    <div class="control-label col-sm-2"></div>
                    <div class="col-sm-4">
                        <button type="submit" class="btn btn-success btn-block">Zmień hasło</button>
                    </div>
                    <div class="col-sm-6"></div>
                </div>
                <div style="clear: both;"></div>

            </form:form>
            <hr>
            <button onclick="window.location.href='/'" type="button" class="btn btn-primary">Wstecz</button>
        </div>
    </div>
</div>
</body>
<script>
    $(document).ready(function () {
       document.getElementById('accountPassword').value = "";
    });
</script>
</html>