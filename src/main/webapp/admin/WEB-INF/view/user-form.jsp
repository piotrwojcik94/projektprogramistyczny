<%--
  Created by IntelliJ IDEA.
  User: Piotr
  Date: 20.03.2018
  Time: 22:49
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<html>
<head>
    <title>Dodaj użytkownika</title>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.css" rel="stylesheet"/>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js" ></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.js" type="text/javascript"></script>
    <link href="/css/main.css" rel="stylesheet" media="screen">
    <link href="/css/settings.css" rel="stylesheet" media="screen">
    <script src="/js/main.js" type="text/javascript" media="screen"></script>
</head>
<body>
<div class="wrapper">
    <nav id="sidebar">
        <div class="sidebar-header">
            <h4>Zalogowany jako: <security:authentication property="principal.username"/></h4>
            <div class="dropdown">
                <div class="setting">
                    <h4 style="display: inline;"><span class="glyphicon glyphicon-user"></span></h4>
                    Ustawienia
                </div>
                <div class="dropdown-content">
                    <a href="${pageContext.request.contextPath}/admin/changeLogin">Zmień login</a>
                    <a href="${pageContext.request.contextPath}/admin/changePassword">Zmień hasło</a>
                </div>
            </div>
        </div>

        <ul class="list-unstyled components">
            <p><b>Opcje użytkownika</b></p>
            <li>
                <a href="${pageContext.request.contextPath}/admin/showFormForAdd">Dodaj użytkownika</a>
            </li>
            <li>
                <a href="${pageContext.request.contextPath}/admin/showFormForAddCard">Dodaj kartę dostępu</a>
            </li>
            <li>
                <a href="${pageContext.request.contextPath}/admin/formForAddHistory">Nasłuchuj czytnik kart</a>
            </li>
        </ul>

        <ul class="list-unstyled CTAs">
            <li><form:form action="${pageContext.request.contextPath}/logout" method="POST">
                <input type="submit" value="Wyloguj się" class="btn btn-danger btn-block">
            </form:form>
            </li>
        </ul>
    </nav>
    <div id="content">

        <nav id="navbar" class="navbar navbar-default">
            <div class="content-fluid">

                <div class="navbar-header">
                    <button type="button" id="sidebarCollapse" class="btn btn-info navbar-btn">
                        <i class="glyphicon glyphicon-align-left"></i>
                    </button>
                </div>

                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right">
                        <li><form:form action="${pageContext.request.contextPath}/logout" method="POST">
                            <button type="submit" class="btn btn-danger btn-block">
                                <span class="glyphicon glyphicon-off"></span> Wyloguj się
                            </button>
                        </form:form></li>
                    </ul>
                </div>
            </div>
        </nav>
        <div class="container">
            <h2>Dodaj usera</h2>
            <hr>

            <form:form id="newUserForm" action="/admin/saveUser" modelAttribute="user" method="POST">

                <form:hidden path="userId"/>
                <div class="form-group" style="padding-bottom: 40px;">
                    <label class="control-label col-sm-2" for="firstName">Imię:</label>
                    <div class="col-sm-4">
                        <form:input path="firstName" class="form-control" autofocus="autofocus" placeholder="Wprowadz imię"/>
                        <form:errors path="firstName" cssClass="error" />
                    </div>
                    <div class="col-sm-6"></div>
                </div>
                <div style="clear: both;"></div>
                <div class="form-group" style="padding-bottom: 40px;">
                    <label class="control-label col-sm-2" for="lastName">Nazwisko:</label>
                    <div class="col-sm-4">
                        <form:input path="lastName" class="form-control" placeholder="Wprowadz nazwisko"/>
                        <form:errors path="lastName" cssClass="error" />
                    </div>
                    <div class="col-sm-6"></div>
                </div>
                <div style="clear: both;"></div>
                <div class="form-group" style="padding-bottom: 40px;">
                    <label class="control-label col-sm-2" for="age">Wiek:</label>
                    <div class="col-sm-4">
                        <form:input path="age" class="form-control" placeholder="Podaj wiek"/>
                        <form:errors path="age" cssClass="error" />
                        <%--<script>--%>
                            <%--var x = document.getElementById('age').value;--%>
                            <%--if(x==='0')--%>
                                <%--document.getElementById('age').value = '';--%>
                        <%--</script>--%>
                    </div>
                    <div class="col-sm-6"></div>
                </div>
                <div style="clear: both;"></div>
                <%--<div class="form-group" style="padding-bottom: 40px;">--%>
                    <%--<label class="control-label col-sm-2" for="accountLogin">Login:</label>--%>
                    <%--<div class="col-sm-4">--%>
                        <form:hidden path="accountLogin" class="form-control" placeholder="Wprowadz hasło"/>
                        <%--<form:errors path="accountLogin" cssClass="error" />--%>
                    <%--</div>--%>
                    <%--<div class="col-sm-6"></div>--%>
                <%--</div>--%>
                <div style="clear: both;"></div>
                <%--<div class="form-group" style="padding-bottom: 40px;">--%>
                    <%--<label class="control-label col-sm-2" for="accountPassword">Hasło:</label>--%>
                    <%--<div class="col-sm-4">--%>
                        <form:hidden path="accountPassword"/>
                        <%--<form:errors path="accountPassword" cssClass="error" />--%>
                    <%--</div>--%>
                    <%--<div class="col-sm-6"></div>--%>
                <%--</div>--%>
                <div style="clear: both;"></div>
                <div class="form-group" style="padding-bottom: 40px;">
                    <label class="control-label col-sm-2" for="userId">Funkcja:</label>
                    <div class="col-sm-4">
                        <form:select path="roleByRoleId.roleId" required="required" class="form-control">
                            <c:forEach var="tempRole" items="${roles}">
                                <form:option value="${tempRole.roleId}">
                                    ${tempRole.role}
                                </form:option>
                            </c:forEach>
                        </form:select>
                    </div>
                    <div class="col-sm-6"></div>
                </div>
                <div class="form-group">
                    <div class="control-label col-sm-2"></div>
                    <div class="col-sm-4">
                        <a class="btn btn-success btn-block"
                           onclick="addNewUser()">
                            Zapisz
                        </a>
                    </div>
                    <div class="col-sm-6"></div>
                </div>
                <div style="clear: both;"></div>

            </form:form>

            <hr>
            <button onclick="window.location.href='/'" type="button" class="btn btn-primary">Wstecz</button>
        </div>
    </div>
</div>
<script>
    function addNewUser() {
        var firstName, lastName;
        firstName = document.getElementById("firstName").value;
        lastName = document.getElementById("lastName").value;
        // sprawdzenie czy user o podanym imieniu i nazwisko istnieje w bazie
        $.ajax({
           url: 'http://localhost:8080/admin/getUsersWithFirstNameAndLastName',
           type: 'GET',
           dataType: 'json',
           data: {'firstName': firstName, 'lastName':lastName},
           success: function (data) {
               if(data>0){
               data= data+1;
               var newLogin = new String(firstName+'.'+lastName+data);
               document.getElementById("accountLogin").value = newLogin.escapeDiacritics();
               document.getElementById("newUserForm").submit();
               } else {
                   var newLogin = new String(firstName+'.'+lastName);
                   document.getElementById("accountLogin").value = newLogin.escapeDiacritics();
                   document.getElementById("newUserForm").submit();
               }
           },
           error: function(){
               alert('Problem z pobraniem')
           }
        });
    }
    String.prototype.escapeDiacritics = function()
    {
        return this.replace(/ą/g, 'a').replace(/Ą/g, 'A')
            .replace(/ć/g, 'c').replace(/Ć/g, 'C')
            .replace(/ę/g, 'e').replace(/Ę/g, 'E')
            .replace(/ł/g, 'l').replace(/Ł/g, 'L')
            .replace(/ń/g, 'n').replace(/Ń/g, 'N')
            .replace(/ó/g, 'o').replace(/Ó/g, 'O')
            .replace(/ś/g, 's').replace(/Ś/g, 'S')
            .replace(/ż/g, 'z').replace(/Ż/g, 'Z')
            .replace(/ź/g, 'z').replace(/Ź/g, 'Z');
    }
</script>
</body>
</html>
