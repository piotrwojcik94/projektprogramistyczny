package com.spring.entity;

import java.util.Collection;
import java.util.Objects;

import javax.persistence.*;

@Entity
@Table(name = "STATES", schema = "pwojcik_projekt")
public class State
{
    private int mStateId;
    private String mState;
    private Collection<Task> mTasksByStateId;
    private Collection<Leave> mLeavesByStateId;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "state_id", nullable = false)
    public int getStateId()
    {
        return mStateId;
    }

    public void setStateId(int stateId)
    {
        mStateId = stateId;
    }

    @Basic
    @Column(name = "state", nullable = false, length = 30)
    public String getState()
    {
        return mState;
    }

    public void setState(String state)
    {
        mState = state;
    }

    @Override
    public boolean equals(Object o)
    {
        if(this == o) return true;
        if(o == null || getClass() != o.getClass()) return false;
        State state = (State) o;
        return mStateId == state.mStateId &&
                Objects.equals(mState, state.mState);
    }

    @Override
    public int hashCode()
    {

        return Objects.hash(mStateId, mState);
    }

    @OneToMany(mappedBy = "stateByStateId", fetch = FetchType.LAZY)
    public Collection<Task> getTasksByStateId()
    {
        return mTasksByStateId;
    }

    public void setTasksByStateId(Collection<Task> tasksByStateId)
    {
        mTasksByStateId = tasksByStateId;
    }

    @OneToMany(mappedBy = "stateByStateId", fetch = FetchType.LAZY)
    public Collection<Leave> getLeavesByStateId()
    {
        return mLeavesByStateId;
    }

    public void setLeavesByStateId(Collection<Leave> leavesByStateId)
    {
        mLeavesByStateId = leavesByStateId;
    }
}
