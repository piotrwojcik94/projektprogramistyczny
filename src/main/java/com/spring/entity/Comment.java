package com.spring.entity;

import java.sql.Timestamp;
import java.util.Objects;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;

@Entity
@Table(name = "COMMENTS", schema = "pwojcik_projekt")
public class Comment
{
    private int mCommentId;
    private String mContent;
    private Timestamp mCommentDate;
    private User mUserByUserId;
    private Task mTaskByTaskId;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "comment_id", nullable = false)
    public int getCommentId()
    {
        return mCommentId;
    }

    public void setCommentId(int commentId)
    {
        mCommentId = commentId;
    }

    @Basic
    @Column(name = "content", nullable = false, length = 100)
    @NotEmpty(message = "Pole 'Komentarz' nie może być puste")
    public String getContent()
    {
        return mContent;
    }

    public void setContent(String content)
    {
        mContent = content;
    }

    @Basic
    @Column(name = "comment_date", nullable = false)
    public Timestamp getCommentDate()
    {
        return mCommentDate;
    }

    public void setCommentDate(Timestamp commentDate)
    {
        mCommentDate = commentDate;
    }

    @Override
    public boolean equals(Object o)
    {
        if(this == o) return true;
        if(o == null || getClass() != o.getClass()) return false;
        Comment comment = (Comment) o;
        return mCommentId == comment.mCommentId &&
                Objects.equals(mContent, comment.mContent) &&
                Objects.equals(mCommentDate, comment.mCommentDate);
    }

    @Override
    public int hashCode()
    {

        return Objects.hash(mCommentId, mContent, mCommentDate);
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id", referencedColumnName = "user_id", nullable = false)
    public User getUserByUserId()
    {
        return mUserByUserId;
    }

    public void setUserByUserId(User userByUserId)
    {
        mUserByUserId = userByUserId;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "task_id", referencedColumnName = "task_id", nullable = false)
    public Task getTaskByTaskId()
    {
        return mTaskByTaskId;
    }

    public void setTaskByTaskId(Task taskByTaskId)
    {
        mTaskByTaskId = taskByTaskId;
    }
}
