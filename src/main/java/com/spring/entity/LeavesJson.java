package com.spring.entity;

public class LeavesJson {

    private String userName;
    private String start;
    private String end;
    private String status;

    public LeavesJson(String userName, String start, String end, String status) {
        this.userName = userName;
        this.start = start;
        this.end = end;
        this.status = status;
    }
}
