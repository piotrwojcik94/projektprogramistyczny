package com.spring.entity;

import java.util.List;

import javax.validation.Valid;

public class ScheduleForm
{
    private List<Schedule> mScheduleList;

    @Valid
    public List<Schedule> getScheduleList()
    {
        return mScheduleList;
    }

    public void setScheduleList(List<Schedule> scheduleList)
    {
        mScheduleList = scheduleList;
    }
}
