package com.spring.entity;

import java.sql.Timestamp;
import java.util.Objects;

import javax.persistence.*;

@Entity
@Table(name = "HISTORY", schema = "pwojcik_projekt")
public class History
{
    private int mHistoryId;
    private Timestamp mEnterDate;
    private Timestamp mExitDate;
    private AccessCard mAccessCardByAccessCardId;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "history_id", nullable = false)
    public int getHistoryId()
    {
        return mHistoryId;
    }

    public void setHistoryId(int historyId)
    {
        mHistoryId = historyId;
    }

    @Basic
    @Column(name = "enter_date", nullable = true)
    public Timestamp getEnterDate()
    {
        return mEnterDate;
    }

    public void setEnterDate(Timestamp enterDate)
    {
        mEnterDate = enterDate;
    }

    @Basic
    @Column(name = "exit_date", nullable = true)
    public Timestamp getExitDate()
    {
        return mExitDate;
    }

    public void setExitDate(Timestamp exitDate)
    {
        mExitDate = exitDate;
    }

    @Override
    public boolean equals(Object o)
    {
        if(this == o) return true;
        if(o == null || getClass() != o.getClass()) return false;
        History history = (History) o;
        return mHistoryId == history.mHistoryId &&
                Objects.equals(mEnterDate, history.mEnterDate) &&
                Objects.equals(mExitDate, history.mExitDate);
    }

    @Override
    public int hashCode()
    {

        return Objects.hash(mHistoryId, mEnterDate, mExitDate);
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "access_card_id", referencedColumnName = "card_id", nullable = false)
    public AccessCard getAccessCardByAccessCardId()
    {
        return mAccessCardByAccessCardId;
    }

    public void setAccessCardByAccessCardId(AccessCard accessCardByAccessCardId)
    {
        mAccessCardByAccessCardId = accessCardByAccessCardId;
    }
}
