package com.spring.entity;

import java.util.Collection;
import java.util.Objects;

import javax.persistence.*;

@Entity
@Table(name = "WEEK_DAYS", schema = "pwojcik_projekt")
public class WeekDay
{
    private int mDayId;
    private String mDayEn;
    private String mDayPl;
    private Collection<Schedule> mSchedulesByDayId;

    @Id
    @Column(name = "day_id", nullable = false)
    public int getDayId()
    {
        return mDayId;
    }

    public void setDayId(int dayId)
    {
        mDayId = dayId;
    }

    @Basic
    @Column(name = "day_en", nullable = false, length = 20)
    public String getDayEn()
    {
        return mDayEn;
    }

    public void setDayEn(String dayEn)
    {
        mDayEn = dayEn;
    }

    @Basic
    @Column(name = "day_pl", nullable = false, length = 20)
    public String getDayPl()
    {
        return mDayPl;
    }

    public void setDayPl(String dayPl)
    {
        mDayPl = dayPl;
    }

    @Override
    public boolean equals(Object o)
    {
        if(this == o) return true;
        if(o == null || getClass() != o.getClass()) return false;
        WeekDay weekDay = (WeekDay) o;
        return mDayId == weekDay.mDayId &&
                Objects.equals(mDayEn, weekDay.mDayEn) &&
                Objects.equals(mDayPl, weekDay.mDayPl);
    }

    @Override
    public int hashCode()
    {

        return Objects.hash(mDayId, mDayEn, mDayPl);
    }

    @OneToMany(mappedBy = "weekDayByWeekDayId", fetch = FetchType.LAZY)
    public Collection<Schedule> getSchedulesByDayId()
    {
        return mSchedulesByDayId;
    }

    public void setSchedulesByDayId(Collection<Schedule> schedulesByDayId)
    {
        mSchedulesByDayId = schedulesByDayId;
    }
}
