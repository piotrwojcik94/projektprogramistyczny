package com.spring.entity;

import org.hibernate.validator.constraints.ScriptAssert;

import java.util.Collection;
import java.util.Date;
import java.util.Objects;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Entity
@Table(name = "TASKS", schema = "pwojcik_projekt")
@ScriptAssert(lang = "javascript",
        script="(_this.startDate == null && _this.endDate == null) || (_this.startDate < _this.endDate)",
        message = "Jedna z dat nie jest ustawiona lub data zakończenia zadania jest przed datą jego rozpoczęcia",
        reportOn = "endDate")
public class Task
{
    private int mTaskId;
    private String mTask;
    private String mDescription;
    private Date mStartDate;
    private Date mEndDate;
    private Boolean mShowed;
    private Collection<Comment> mCommentsByTaskId;
    private User mUserByAuthorId;
    private User mUserByUserId;
    private State mStateByStateId;

    @Id
    @Column(name = "task_id", nullable = false)
    public int getTaskId()
    {
        return mTaskId;
    }

    public void setTaskId(int taskId)
    {
        mTaskId = taskId;
    }

    @Basic
    @Column(name = "task", nullable = false, length = 200)
    @NotEmpty(message = "Pole 'Zadanie' nie może być puste")
    @Size(min = 6, message = "Nazwa zadania powinna zawierać minimum 6 znaków")
    public String getTask()
    {
        return mTask;
    }

    public void setTask(String task)
    {
        mTask = task;
    }

    @Basic
    @Column(name = "description", nullable = true, length = 500)
    public String getDescription()
    {
        return mDescription;
    }

    public void setDescription(String description)
    {
        mDescription = description;
    }

    @Basic
    @Column(name = "start_date", nullable = true)
    @Temporal(TemporalType.TIMESTAMP)
    public Date getStartDate()
    {
        return mStartDate;
    }

    public void setStartDate(Date startDate)
    {
        mStartDate = startDate;
    }

    @Basic
    @Column(name = "end_date", nullable = true)
    @Temporal(TemporalType.TIMESTAMP)
    public Date getEndDate()
    {
        return mEndDate;
    }

    public void setEndDate(Date endDate)
    {
        mEndDate = endDate;
    }

    @Basic
    @Column(name = "showed", nullable = true)
    public Boolean getShowed()
    {
        return mShowed;
    }

    public void setShowed(Boolean showed)
    {
        mShowed = showed;
    }

    @Override
    public boolean equals(Object o)
    {
        if(this == o) return true;
        if(o == null || getClass() != o.getClass()) return false;
        Task task = (Task) o;
        return mTaskId == task.mTaskId &&
                Objects.equals(mTask, task.mTask) &&
                Objects.equals(mDescription, task.mDescription) &&
                Objects.equals(mStartDate, task.mStartDate) &&
                Objects.equals(mEndDate, task.mEndDate) &&
                Objects.equals(mShowed, task.mShowed);
    }

    @Override
    public int hashCode()
    {

        return Objects.hash(mTaskId, mTask, mDescription, mStartDate, mEndDate, mShowed);
    }

    @OneToMany(mappedBy = "taskByTaskId", fetch = FetchType.LAZY)
    public Collection<Comment> getCommentsByTaskId()
    {
        return mCommentsByTaskId;
    }

    public void setCommentsByTaskId(Collection<Comment> commentsByTaskId)
    {
        mCommentsByTaskId = commentsByTaskId;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "author_id", referencedColumnName = "user_id")
    public User getUserByAuthorId()
    {
        return mUserByAuthorId;
    }

    public void setUserByAuthorId(User userByAuthorId)
    {
        mUserByAuthorId = userByAuthorId;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id", referencedColumnName = "user_id")
    public User getUserByUserId()
    {
        return mUserByUserId;
    }

    public void setUserByUserId(User userByUserId)
    {
        mUserByUserId = userByUserId;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "state_id", referencedColumnName = "state_id")
    public State getStateByStateId()
    {
        return mStateByStateId;
    }

    public void setStateByStateId(State stateByStateId)
    {
        mStateByStateId = stateByStateId;
    }
}
