package com.spring.entity;

public class HistoryJson
{
    private String title;
    private String start;
    private String stop;
    private String timeDifference;

    public HistoryJson(String title, String start, String stop, String timeDifference)
    {
        this.title = title;
        this.start = start;
        this.stop = stop;
        this.timeDifference = timeDifference;
    }
}
