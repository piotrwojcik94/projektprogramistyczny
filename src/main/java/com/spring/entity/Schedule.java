package com.spring.entity;

import org.hibernate.validator.constraints.ScriptAssert;

import java.sql.Time;
import java.util.Objects;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "SCHEDULE", schema = "pwojcik_projekt")
@ScriptAssert(lang = "javascript",
        script="(_this.enterHour == null && _this.exitHour == null) || ((_this.enterHour >= '06:00:00') && (_this.enterHour < '22:00:00') && (_this.exitHour > '06:00:00') && (_this.exitHour <= '22:00:00') && (_this.enterHour < _this.exitHour))",
        message = "Jedna z godzin nie jest ustawiona lub godzina wyjścia jest przed godziną wejścia.\nDodatkowo godziny wejścia i wyjścia powinny zawierać się między 6 a 22.",
        reportOn = "exitHour")
public class Schedule
{
    private int mScheduleId;
    private Time mEnterHour;
    private Time mExitHour;
    private User mUserByUserId;
    private WeekDay mWeekDayByWeekDayId;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "schedule_id", nullable = false)
    public int getScheduleId()
    {
        return mScheduleId;
    }

    public void setScheduleId(int scheduleId)
    {
        mScheduleId = scheduleId;
    }

    @Basic
    @Column(name = "enter_hour", nullable = true)
    public Time getEnterHour()
    {
        return mEnterHour;
    }

    public void setEnterHour(Time enterHour)
    {
        mEnterHour = enterHour;
    }

    @Basic
    @Column(name = "exit_hour", nullable = true)
    public Time getExitHour()
    {
        return mExitHour;
    }

    public void setExitHour(Time exitHour)
    {
        mExitHour = exitHour;
    }

    @Override
    public boolean equals(Object o)
    {
        if(this == o) return true;
        if(o == null || getClass() != o.getClass()) return false;
        Schedule schedule = (Schedule) o;
        return mScheduleId == schedule.mScheduleId &&
                Objects.equals(mEnterHour, schedule.mEnterHour) &&
                Objects.equals(mExitHour, schedule.mExitHour);
    }

    @Override
    public int hashCode()
    {

        return Objects.hash(mScheduleId, mEnterHour, mExitHour);
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id", referencedColumnName = "user_id", nullable = false)
    public User getUserByUserId()
    {
        return mUserByUserId;
    }

    public void setUserByUserId(User userByUserId)
    {
        mUserByUserId = userByUserId;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "week_day_id", referencedColumnName = "day_id", nullable = false)
    public WeekDay getWeekDayByWeekDayId()
    {
        return mWeekDayByWeekDayId;
    }

    public void setWeekDayByWeekDayId(WeekDay weekDayByWeekDayId)
    {
        mWeekDayByWeekDayId = weekDayByWeekDayId;
    }
}
