package com.spring.entity;

import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.Objects;

import javax.persistence.*;

@Entity
@Table(name = "LEAVES", schema = "pwojcik_projekt")
public class Leave
{
    private int mLeaveId;
    private Date mDateFrom;
    private Date mDateTo;
    private User mUserByUserId;
    private State mStateByStateId;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "leave_id", nullable = false)
    public int getLeaveId()
    {
        return mLeaveId;
    }

    public void setLeaveId(int leaveId)
    {
        mLeaveId = leaveId;
    }

    @Basic
    @Column(name = "date_from", nullable = false)
    @Temporal(TemporalType.DATE)
    @DateTimeFormat(pattern = "yyyy/MM/dd")
    public Date getDateFrom()
    {
        return mDateFrom;
    }

    public void setDateFrom(Date dateFrom)
    {
        mDateFrom = dateFrom;
    }

    @Basic
    @Column(name = "date_to", nullable = false)
    @Temporal(TemporalType.DATE)
    @DateTimeFormat(pattern = "yyyy/MM/dd")
    public Date getDateTo()
    {
        return mDateTo;
    }

    public void setDateTo(Date dateTo)
    {
        mDateTo = dateTo;
    }

    @Override
    public boolean equals(Object o)
    {
        if(this == o) return true;
        if(o == null || getClass() != o.getClass()) return false;
        Leave leave = (Leave) o;
        return mLeaveId == leave.mLeaveId &&
                Objects.equals(mDateFrom, leave.mDateFrom) &&
                Objects.equals(mDateTo, leave.mDateTo);
    }

    @Override
    public int hashCode()
    {

        return Objects.hash(mLeaveId, mDateFrom, mDateTo);
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id", referencedColumnName = "user_id", nullable = false)
    public User getUserByUserId()
    {
        return mUserByUserId;
    }

    public void setUserByUserId(User userByUserId)
    {
        mUserByUserId = userByUserId;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "state_id", referencedColumnName = "state_id")
    public State getStateByStateId()
    {
        return mStateByStateId;
    }

    public void setStateByStateId(State stateByStateId)
    {
        mStateByStateId = stateByStateId;
    }
}
