package com.spring.entity;

import java.util.Collection;
import java.util.Objects;

import javax.persistence.*;

@Entity
@Table(name = "ROLES", schema = "pwojcik_projekt")
public class Role
{
    private int mRoleId;
    private String mRole;
    private String mDescription;
    private Collection<User> mUsersByRoleId;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "role_id", nullable = false)
    public int getRoleId()
    {
        return mRoleId;
    }

    public void setRoleId(int roleId)
    {
        mRoleId = roleId;
    }

    @Basic
    @Column(name = "role", nullable = false, length = 60)
    public String getRole()
    {
        return mRole;
    }

    public void setRole(String role)
    {
        mRole = role;
    }

    @Basic
    @Column(name = "description", nullable = true, length = 200)
    public String getDescription()
    {
        return mDescription;
    }

    public void setDescription(String description)
    {
        mDescription = description;
    }

    @Override
    public boolean equals(Object o)
    {
        if(this == o) return true;
        if(o == null || getClass() != o.getClass()) return false;
        Role role = (Role) o;
        return mRoleId == role.mRoleId &&
                Objects.equals(mRole, role.mRole) &&
                Objects.equals(mDescription, role.mDescription);
    }

    @Override
    public int hashCode()
    {

        return Objects.hash(mRoleId, mRole, mDescription);
    }

    @OneToMany(mappedBy = "roleByRoleId", fetch = FetchType.LAZY)
    public Collection<User> getUsersByRoleId()
    {
        return mUsersByRoleId;
    }

    public void setUsersByRoleId(Collection<User> usersByRoleId)
    {
        mUsersByRoleId = usersByRoleId;
    }
}
