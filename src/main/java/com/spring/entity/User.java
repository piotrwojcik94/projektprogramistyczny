package com.spring.entity;

import org.hibernate.validator.constraints.Range;

import java.util.Collection;
import java.util.Objects;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "USERS", schema = "pwojcik_projekt")
public class User
{
    private int mUserId;
    private String mFirstName;
    private String mLastName;
    private Integer mAge;
    private String mAccountLogin;
    private String mAccountPassword;
    private AccessCard mAccessCardByUserId;
    private Collection<Comment> mCommentsByUserId;
    private Collection<Leave> mLeavesByUserId;
    private Collection<Schedule> mSchedulesByUserId;
    private Collection<Task> mTasksByAuthorId;
    private Collection<Task> mTasksByUserId;
    private Role mRoleByRoleId;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_id", nullable = false)
    public int getUserId()
    {
        return mUserId;
    }

    public void setUserId(int userId)
    {
        mUserId = userId;
    }

    @Basic
    @Column(name = "first_name", nullable = false, length = 30)
    @NotEmpty(message = "Pole 'Imię' nie może być puste")
    @Size(min = 3, max = 20, message = "Imię powinno mieć od 3 do 20 znaków")
    public String getFirstName()
    {
        return mFirstName;
    }

    public void setFirstName(String firstName)
    {
        mFirstName = firstName;
    }

    @Basic
    @Column(name = "last_name", nullable = false, length = 30)
    @NotEmpty(message = "Pole 'Nazwisko' nie może być puste")
    @Size(min = 3, max = 20, message = "Nazwisko powinno mieć od 3 do 20 znaków")
    public String getLastName()
    {
        return mLastName;
    }

    public void setLastName(String lastName)
    {
        mLastName = lastName;
    }

    @Basic
    @Column(name = "age", nullable = false)
    @NotNull(message = "Pole 'Wiek' nie może być puste")
    @Range(min = 18, max = 125, message = "Wiek nie może być mniejszy niż 18 i większy niż 125 lat")
    public Integer getAge()
    {
        return mAge;
    }

    public void setAge(Integer age)
    {
        mAge = age;
    }

    @Basic
    @NotNull(message = "Pole 'Login' nie może być puste")
    @Column(name = "account_login", nullable = false)
    public String getAccountLogin()
    {
        return mAccountLogin;
    }

    public void setAccountLogin(String accountLogin)
    {
        mAccountLogin = accountLogin;
    }

    @Basic
    @Column(name = "account_password", nullable = false)
    public String getAccountPassword()
    {
        return mAccountPassword;
    }

    public void setAccountPassword(String accountPassword)
    {
        mAccountPassword = accountPassword;
    }

    @Override
    public boolean equals(Object o)
    {
        if(this == o) return true;
        if(o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return mUserId == user.mUserId &&
                mAge == user.mAge &&
                Objects.equals(mFirstName, user.mFirstName) &&
                Objects.equals(mLastName, user.mLastName) &&
                Objects.equals(mAccountLogin, user.mAccountLogin) &&
                Objects.equals(mAccountPassword, user.mAccountPassword);
    }

    @Override
    public int hashCode()
    {

        return Objects.hash(mUserId, mFirstName, mLastName, mAge, mAccountLogin, mAccountPassword);
    }

    @OneToOne(mappedBy = "userByUserId", fetch = FetchType.EAGER)
    public AccessCard getAccessCardByUserId()
    {
        return mAccessCardByUserId;
    }

    public void setAccessCardByUserId(AccessCard accessCardByUserId)
    {
        mAccessCardByUserId = accessCardByUserId;
    }

    @OneToMany(mappedBy = "userByUserId", fetch = FetchType.LAZY)
    public Collection<Comment> getCommentsByUserId()
    {
        return mCommentsByUserId;
    }

    public void setCommentsByUserId(Collection<Comment> commentsByUserId)
    {
        mCommentsByUserId = commentsByUserId;
    }

    @OneToMany(mappedBy = "userByUserId", fetch = FetchType.LAZY)
    public Collection<Leave> getLeavesByUserId()
    {
        return mLeavesByUserId;
    }

    public void setLeavesByUserId(Collection<Leave> leavesByUserId)
    {
        mLeavesByUserId = leavesByUserId;
    }

    @OneToMany(mappedBy = "userByUserId", fetch = FetchType.LAZY)
    public Collection<Schedule> getSchedulesByUserId()
    {
        return mSchedulesByUserId;
    }

    public void setSchedulesByUserId(Collection<Schedule> schedulesByUserId)
    {
        mSchedulesByUserId = schedulesByUserId;
    }

    @OneToMany(mappedBy = "userByAuthorId", fetch = FetchType.LAZY)
    public Collection<Task> getTasksByAuthorId()
    {
        return mTasksByAuthorId;
    }

    public void setTasksByAuthorId(Collection<Task> tasksByAuthorId)
    {
        mTasksByAuthorId = tasksByAuthorId;
    }

    @OneToMany(mappedBy = "userByUserId", fetch = FetchType.LAZY)
    public Collection<Task> getTasksByUserId()
    {
        return mTasksByUserId;
    }

    public void setTasksByUserId(Collection<Task> tasksByUserId)
    {
        mTasksByUserId = tasksByUserId;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "role_id", referencedColumnName = "role_id", nullable = false)
    public Role getRoleByRoleId()
    {
        return mRoleByRoleId;
    }

    public void setRoleByRoleId(Role roleByRoleId)
    {
        mRoleByRoleId = roleByRoleId;
    }
}
