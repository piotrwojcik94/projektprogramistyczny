package com.spring.entity;

import java.util.Collection;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "ACCESS_CARDS", schema = "pwojcik_projekt")
public class AccessCard
{
    private Integer mCardId;
    private User mUserByUserId;
    private Collection<History> mHistoriesByCardId;

    @Id
    @Column(name = "card_id", nullable = false)
    @NotNull(message = "Pole 'Id karty' nie może być puste")
    public Integer getCardId()
    {
        return mCardId;
    }

    public void setCardId(Integer cardId)
    {
        mCardId = cardId;
    }

    @Override
    public boolean equals(Object o)
    {
        if(this == o) return true;
        if(o == null || getClass() != o.getClass()) return false;
        AccessCard that = (AccessCard) o;
        return mCardId == that.mCardId;
    }

    @Override
    public int hashCode()
    {

        return Objects.hash(mCardId);
    }

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id", referencedColumnName = "user_id")
    public User getUserByUserId()
    {
        return mUserByUserId;
    }

    public void setUserByUserId(User userByUserId)
    {
        mUserByUserId = userByUserId;
    }

    @OneToMany(mappedBy = "accessCardByAccessCardId", fetch = FetchType.LAZY)
    public Collection<History> getHistoriesByCardId()
    {
        return mHistoriesByCardId;
    }

    public void setHistoriesByCardId(Collection<History> historiesByCardId)
    {
        mHistoriesByCardId = historiesByCardId;
    }
}
