package com.spring.service;

import com.spring.dao.WeekDayDAO;
import com.spring.entity.WeekDay;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class WeekDayServiceImpl implements WeekDayService {

    @Autowired
    private WeekDayDAO mWeekDayDAO;

    @Override
    @Transactional
    public List<WeekDay> getWorkdays()
    {
        return mWeekDayDAO.getWorkdays();
    }
}
