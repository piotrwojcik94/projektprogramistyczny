package com.spring.service;

import com.spring.dao.ScheduleDAO;
import com.spring.entity.Schedule;
import com.spring.entity.User;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

@Service
public class ScheduleServiceImpl implements ScheduleService
{
    @Autowired
    private ScheduleDAO mScheduleDAO;

    @Override
    @Transactional
    public void saveSchedule(Schedule schedule)
    {
        mScheduleDAO.saveSchedule(schedule);
    }

    @Override
    @Transactional
    public void saveWeekScheduleForUser(User user, List<Schedule> schedule)
    {
        mScheduleDAO.saveWeekScheduleForUser(user, schedule);
    }

    @Override
    @Transactional
    public List<Schedule> getScheduleByUserId(int userId)
    {
        return mScheduleDAO.getScheduleByUserId(userId);
    }

    @Override
    @Transactional
    public Map<User, List<Schedule>> getWholeSchedule()
    {
        return mScheduleDAO.getWholeSchedule();
    }


}
