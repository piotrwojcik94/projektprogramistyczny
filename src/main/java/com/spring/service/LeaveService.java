package com.spring.service;

import com.spring.entity.Leave;
import com.spring.entity.LeavesJson;
import com.spring.entity.User;

import java.util.List;

public interface LeaveService {
    List<Leave> getLeavesByUserId(int userId);

    void saveLeave(Leave leave);

    List<LeavesJson> getLeavesJsonForAllUsers();

    List<Leave> getAllLeaves(Integer offset, Integer maxResults);

    void deleteLeave(int id);

    void acceptLeave(int id);

    void kickLeave(int id);

    Long count();

    List<LeavesJson> getLeaveForUser(User user, int year);

    List<Leave> getAllLeavesForEmployee(int userId, Integer offset, Integer maxResults);

    Long countForEmployee(int userId);

    Long getCountLeavesShowed();
}
