package com.spring.service;

import com.spring.entity.Role;

import java.util.List;

public interface RoleService
{
    void saveRole(Role role);

    List<Role> getRoles();

    void deleteRole(int id);

    Role getRole(int id);
}
