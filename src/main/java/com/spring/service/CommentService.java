package com.spring.service;

import com.spring.entity.Comment;

import java.util.List;

public interface CommentService {
    List<Comment> getCommentsByTaskId(int id);

    void saveComment(Comment comment);
}
