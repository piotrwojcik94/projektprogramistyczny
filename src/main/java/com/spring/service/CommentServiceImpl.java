package com.spring.service;

import com.spring.dao.CommentDAO;
import com.spring.entity.Comment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class CommentServiceImpl implements CommentService {

    @Autowired
    private CommentDAO mCommentDAO;

    @Override
    @Transactional
    public List<Comment> getCommentsByTaskId(int id) {
        return mCommentDAO.getCommentByTaskId(id);
    }

    @Override
    @Transactional
    public void saveComment(Comment comment) {
        mCommentDAO.saveComment(comment);
    }
}
