package com.spring.service;

import com.spring.dao.RoleDAO;
import com.spring.entity.Role;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class RoleServiceImpl implements RoleService
{

    @Autowired
    private RoleDAO mRoleDAO;

    @Override
    @Transactional
    public void saveRole(Role role)
    {
        mRoleDAO.saveRole(role);
    }

    @Override
    @Transactional
    public List<Role> getRoles()
    {
        return mRoleDAO.getRoles();
    }

    @Override
    @Transactional
    public void deleteRole(int id)
    {
        mRoleDAO.deleteRole(id);
    }

    @Override
    @Transactional
    public Role getRole(int id)
    {
        return mRoleDAO.getRole(id);
    }
}
