package com.spring.service;

import com.spring.dao.TaskDAO;
import com.spring.entity.Task;
import com.spring.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class TaskServiceImpl implements TaskService {

    @Autowired
    TaskDAO mTaskDAO;

    @Override
    @Transactional
    public void saveTask(Task task) {
        mTaskDAO.saveTask(task);
    }

    @Override
    @Transactional
    public List<Task> getTaskByManager(User user) {
        return mTaskDAO.getTaskByManager(user);
    }

    @Override
    @Transactional
    public Task getTaskById(int id) {
        return mTaskDAO.getTaskById(id);
    }

    @Override
    @Transactional
    public List<Task> getTaskForEmployee(User user) {
        return mTaskDAO.getTaskForEmployee(user);
    }

    @Override
    @Transactional
    public List<Task> getListTask(int userId, Integer offset, Integer maxResults) {
        return mTaskDAO.getListTask(userId, offset, maxResults);
    }

    @Override
    @Transactional
    public Long count(int userId) {
        return mTaskDAO.count(userId);
    }

    @Override
    @Transactional
    public void deleteTaskById(int id) {
        mTaskDAO.deleteTaskById(id);
    }

    @Override
    @Transactional
    public List<Task> getListTaskForEmployee(int userId, Integer offset, Integer maxResults) {
        return mTaskDAO.getListTaskForEmployee(userId,offset,maxResults);
    }

    @Override
    @Transactional
    public Long countForEmployee(int userId) {
        return mTaskDAO.countForEmployee(userId);
    }

    @Override
    @Transactional
    public Long getCountTaskShowed(int userId) {
        return mTaskDAO.getCountTaskShowed(userId);
    }

}
