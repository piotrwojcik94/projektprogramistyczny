package com.spring.service;

import com.spring.dao.HistoryDAO;
import com.spring.entity.History;
import com.spring.entity.HistoryJson;
import com.spring.entity.User;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

@Service
public class HistoryServiceImpl implements HistoryService
{
    @Autowired
    private HistoryDAO mHistoryDAO;

    @Override
    @Transactional
    public void saveHistory(History history)
    {
        mHistoryDAO.saveHistory(history);
    }

    @Override
    @Transactional
    public Map<User, List<History>> getWholeHistory()
    {
        return mHistoryDAO.getWholeHistory();
    }

    @Override
    @Transactional
    public List<History> getHistoryByUser(int userId)
    {
        return mHistoryDAO.getHistoryByUser(userId);
    }

    @Override
    @Transactional
    public List<HistoryJson> getHistoryJsonByUser(int userId)
    {
        return mHistoryDAO.getHistoryJsonByUser(userId);
    }

    @Override
    @Transactional
    public History getLastHistoryByAccessCardId(int accessCardId)
    {
        return mHistoryDAO.getLastHistoryByAccessCardId(accessCardId);
    }
}
