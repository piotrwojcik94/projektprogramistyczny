package com.spring.service;

import com.spring.entity.WeekDay;

import java.util.List;

public interface WeekDayService
{
    List<WeekDay> getWorkdays();
}
