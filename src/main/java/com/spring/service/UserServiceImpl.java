package com.spring.service;

import com.spring.dao.UserDAO;
import com.spring.entity.User;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDAO mUserDAO;

    @Override
    @Transactional
    public void saveUser(User user)
    {
        mUserDAO.saveUser(user);
    }

    @Override
    @Transactional
    public List<User> getUsers(Integer offset, Integer maxResults)
    {
        return mUserDAO.getUsers(offset, maxResults);
    }

    @Override
    @Transactional
    public void deleteUser(int id)
    {
        mUserDAO.deleteUser(id);
    }

    @Override
    @Transactional
    public User getUser(int id)
    {
        return mUserDAO.getUser(id);
    }

    @Override
    @Transactional
    public List<User> getUsersWithoutCard() {
        return mUserDAO.getUsersWithoutCard();
    }

    @Override
    @Transactional
    public User getUserByLogin(String login)
    {
        return mUserDAO.getUserByLogin(login);
    }

    @Override
    @Transactional
    public List<User> getUsersLikeEmployee() {
        return mUserDAO.getUsersLikeEmployee();
    }

    @Override
    @Transactional
    public List<User> getUsersWithoutOne(int id) {
        return mUserDAO.getUsersWithoutOne(id);
    }

    @Override
    @Transactional
    public Long getCount() {
        return mUserDAO.getCount();
    }

    @Override
    @Transactional
    public Long getUsersCountWithFirstNameAndLastName(String firstName, String lastName) {
        return mUserDAO.getUsersCountWithFirstNameAndLastName(firstName, lastName);
    }

}
