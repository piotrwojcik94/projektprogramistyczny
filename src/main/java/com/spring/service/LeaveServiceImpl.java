package com.spring.service;

import com.spring.dao.LeaveDAO;
import com.spring.entity.Leave;
import com.spring.entity.LeavesJson;
import com.spring.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class LeaveServiceImpl implements LeaveService {

    @Autowired
    private LeaveDAO mLeaveDAO;

    @Override
    @Transactional
    public List<Leave> getLeavesByUserId(int userId) {
        return mLeaveDAO.getLeavesByUserId(userId);
    }

    @Override
    @Transactional
    public void saveLeave(Leave leave) {
        mLeaveDAO.saveLeave(leave);
    }

    @Override
    @Transactional
    public List<LeavesJson> getLeavesJsonForAllUsers() {
        return mLeaveDAO.getLeavesJsonForAllUsers();
    }

    @Override
    @Transactional
    public List<Leave> getAllLeaves(Integer offset, Integer maxResults) {
        return mLeaveDAO.getAllLeaves(offset, maxResults);
    }

    @Override
    @Transactional
    public void deleteLeave(int id) {
        mLeaveDAO.deleteLeave(id);
    }

    @Override
    @Transactional
    public void acceptLeave(int id) {
        mLeaveDAO.acceptLeave(id);
    }

    @Override
    @Transactional
    public void kickLeave(int id) {
        mLeaveDAO.kickLeave(id);
    }

    @Override
    @Transactional
    public Long count() {
        return mLeaveDAO.count();
    }

    @Override
    @Transactional
    public List<LeavesJson> getLeaveForUser(User user, int year) {
        return mLeaveDAO.getLeaveForUser(user,year);
    }

    @Override
    @Transactional
    public List<Leave> getAllLeavesForEmployee(int userId, Integer offset, Integer maxResults) {
        return mLeaveDAO.getAllLeavesForEmployee(userId, offset, maxResults);
    }

    @Override
    @Transactional
    public Long countForEmployee(int userId) {
        return mLeaveDAO.countForEmployee(userId);
    }

    @Override
    @Transactional
    public Long getCountLeavesShowed() {
        return mLeaveDAO.getCountLeavesShowed();
    }

}
