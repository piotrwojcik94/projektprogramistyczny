package com.spring.service;

import com.spring.entity.AccessCard;
import com.spring.entity.User;

import java.util.List;

public interface AccessCardService {
    List<AccessCard> getAccessCards();

    void addUserToCard(User user);

    AccessCard getCard(int Id);

    void saveCard(AccessCard accessCard);

    List<AccessCard> getCardsWithoutUsers();
}
