package com.spring.service;

import com.spring.dao.StateDAO;
import com.spring.entity.State;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class StateServiceImpl implements StateService {

    @Autowired
    private StateDAO mStateDAO;

    @Override
    @Transactional
    public List<State> getStates() {
        return mStateDAO.getState();
    }

    @Override
    @Transactional
    public State getStateByStateId(int id) {
        return mStateDAO.getStateByStateId(id);
    }
}
