package com.spring.service;

import com.spring.entity.State;

import java.util.List;

public interface StateService {
    List<State> getStates();

    State getStateByStateId(int id);
}
