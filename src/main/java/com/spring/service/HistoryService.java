package com.spring.service;

import com.spring.entity.History;
import com.spring.entity.HistoryJson;
import com.spring.entity.User;

import java.util.List;
import java.util.Map;

public interface HistoryService
{
    void saveHistory(History history);

    Map<User, List<History>> getWholeHistory();

    List<History> getHistoryByUser(int userId);

    List<HistoryJson> getHistoryJsonByUser(int userId);

    History getLastHistoryByAccessCardId(int accessCardId);
}
