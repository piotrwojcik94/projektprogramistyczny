package com.spring.service;

import com.spring.dao.AccessCardDAO;
import com.spring.entity.AccessCard;
import com.spring.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class AccessCardServiceImpl implements AccessCardService {

    @Autowired
    private AccessCardDAO mAccessCardDAO;

    @Override
    @Transactional
    public List<AccessCard> getAccessCards() {
        return mAccessCardDAO.getAccessCards();
    }

    @Override
    @Transactional
    public void addUserToCard(User user) {
        mAccessCardDAO.addUserToCard(user);
    }

    @Override
    @Transactional
    public AccessCard getCard(int Id) {
        return mAccessCardDAO.getCard(Id);
    }

    @Override
    @Transactional
    public void saveCard(AccessCard accessCard) {
        mAccessCardDAO.saveCard(accessCard);
    }

    @Override
    @Transactional
    public List<AccessCard> getCardsWithoutUsers() {
        return mAccessCardDAO.getCardsWithoutUsers();
    }
}
