package com.spring.service;

import com.spring.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

public class UserDetails implements UserDetailsService {

    @Autowired
    private UserService mUserService;

    @Override
    public org.springframework.security.core.userdetails.UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        User user = mUserService.getUserByLogin(s);
        org.springframework.security.core.userdetails.User.UserBuilder userBuilder;
        userBuilder = org.springframework.security.core.userdetails.User.withUsername(user.getAccountLogin());
        userBuilder.password(user.getAccountPassword());
        userBuilder.roles(user.getRoleByRoleId().getRole());
        return userBuilder.build();
    }
}
