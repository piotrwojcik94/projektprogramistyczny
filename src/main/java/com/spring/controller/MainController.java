package com.spring.controller;

import com.spring.entity.*;
import com.spring.service.*;
import com.google.gson.Gson;
import com.spring.entity.AccessCard;
import com.spring.entity.Comment;
import com.spring.entity.History;
import com.spring.entity.HistoryJson;
import com.spring.entity.Role;
import com.spring.entity.Schedule;
import com.spring.entity.ScheduleForm;
import com.spring.entity.State;
import com.spring.entity.Task;
import com.spring.entity.User;
import com.spring.entity.WeekDay;
import com.spring.service.AccessCardService;
import com.spring.service.CommentService;
import com.spring.service.HistoryService;
import com.spring.service.RoleService;
import com.spring.service.ScheduleService;
import com.spring.service.StateService;
import com.spring.service.TaskService;
import com.spring.service.UserService;
import com.spring.service.WeekDayService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import java.sql.Time;
import java.sql.Timestamp;
import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

@Controller
public class MainController
{

    @Autowired
    private UserService mUserService;

    @Autowired
    private RoleService mRoleService;

    @Autowired
    private AccessCardService mAccessCardService;

    @Autowired
    private ScheduleService mScheduleService;

    @Autowired
    private HistoryService mHistoryService;

    @Autowired
    private TaskService mTaskService;

    @Autowired
    private StateService mStateService;

    @Autowired
    private CommentService mCommentService;

    @Autowired
    private WeekDayService mWeekDayService;

    @Autowired
    private LeaveService mLeaveService;

    @Autowired
    private BCryptPasswordEncoder mBCryptPasswordEncoder;

    @InitBinder
    public void initBinder(WebDataBinder binder)
    {
        binder.registerCustomEditor(String.class, new StringTrimmerEditor(true));
        binder.registerCustomEditor(Timestamp.class, new StringTrimmerEditor(true));
        binder.registerCustomEditor(Time.class, new StringTrimmerEditor(true));
        binder.registerCustomEditor(Date.class, new StringTrimmerEditor(true));
    }

    @GetMapping("/admin")
    public String listUsers(Model theModel, Integer offset, Integer maxResults)
    {
        theModel.addAttribute("users", mUserService.getUsers(offset, maxResults));
        theModel.addAttribute("count", mUserService.getCount());
        theModel.addAttribute("offset", offset);
        return "home";
    }

    @GetMapping("/admin/showFormForAdd")
    public String showFormForAdd(Model theModel)
    {
        User user = new User();
        theModel.addAttribute("user", user);
        // dodanie roli usera
        List<Role> roles = mRoleService.getRoles();
        theModel.addAttribute("roles", roles);
        return "user-form";
    }

    @PostMapping("/admin/saveUser")
    public String saveUser(@Valid @ModelAttribute("user") User user, BindingResult result, Model model)
    {
        if(result.hasErrors())
        {
            List<Role> roles = mRoleService.getRoles();
            model.addAttribute("roles", roles);

            return "user-form";
        }

        user.setAccountPassword(mBCryptPasswordEncoder.encode("haslo123"));
        mUserService.saveUser(user);

        return "redirect:/admin";
    }

    @GetMapping("/admin/changeLogin")
    public String showFormForChangeLoginByAdmin(Model theModel)
    {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = mUserService.getUserByLogin(auth.getName());
        theModel.addAttribute("user",user);
        return "new-login";
    }

    @GetMapping("/admin/changePassword")
    public String showFormForChangePasswordByAdmin(Model theModel)
    {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = mUserService.getUserByLogin(auth.getName());
        theModel.addAttribute("user",user);
        return "new-password";
    }

    @PostMapping("/admin/saveNewLogin")
    public String saveNewLoginByAdmin(@Valid @ModelAttribute("user") User user, BindingResult result, HttpServletRequest request)
    {
        mUserService.saveUser(user);
        HttpSession httpSession = request.getSession();
        httpSession.invalidate();
        return "redirect:/?logout";
    }

    @PostMapping("/admin/saveNewPassword")
    public String saveNewPasswordByAdmin(@Valid @ModelAttribute("user") User user, BindingResult result, HttpServletRequest request)
    {
        user.setAccountPassword(mBCryptPasswordEncoder.encode(user.getAccountPassword()));
        mUserService.saveUser(user);
        return "redirect:/";
    }

    @GetMapping("/managers/changeLogin")
    public String showFormForChangeLoginByManager(Model theModel)
    {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = mUserService.getUserByLogin(auth.getName());
        theModel.addAttribute("user",user);
        return "new-login";
    }

    @GetMapping("/managers/changePassword")
    public String showFormForChangePasswordByManager(Model theModel)
    {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = mUserService.getUserByLogin(auth.getName());
        theModel.addAttribute("user",user);
        return "new-password";
    }

    @PostMapping("/managers/saveNewLogin")
    public String saveNewLoginByManager(@Valid @ModelAttribute("user") User user, BindingResult result, HttpServletRequest request)
    {
        mUserService.saveUser(user);
        HttpSession httpSession = request.getSession();
        httpSession.invalidate();
        return "redirect:/?logout";
    }

    @PostMapping("/managers/saveNewPassword")
    public String saveNewPasswordByManager(@Valid @ModelAttribute("user") User user, BindingResult result, HttpServletRequest request)
    {
        user.setAccountPassword(mBCryptPasswordEncoder.encode(user.getAccountPassword()));
        mUserService.saveUser(user);
        return "redirect:/";
    }

    @GetMapping("/employees/changeLogin")
    public String showFormForChangeLoginByEmployee(Model theModel)
    {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = mUserService.getUserByLogin(auth.getName());
        theModel.addAttribute("user",user);
        return "new-login";
    }

    @GetMapping("/employees/changePassword")
    public String showFormForChangePasswordByEmployee(Model theModel)
    {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = mUserService.getUserByLogin(auth.getName());
        theModel.addAttribute("user",user);
        return "new-password";
    }

    @PostMapping("/employees/saveNewLogin")
    public String saveNewLoginByEmployee(@Valid @ModelAttribute("user") User user, BindingResult result, HttpServletRequest request)
    {
        mUserService.saveUser(user);
        HttpSession httpSession = request.getSession();
        httpSession.invalidate();
        return "redirect:/?logout";
    }

    @PostMapping("/employees/saveNewPassword")
    public String saveNewPasswordByEmployee(@Valid @ModelAttribute("user") User user, BindingResult result, HttpServletRequest request)
    {
        user.setAccountPassword(mBCryptPasswordEncoder.encode(user.getAccountPassword()));
        mUserService.saveUser(user);
        return "redirect:/";
    }

    @PostMapping("/admin/updateUser")
    public String updateUser(@Valid @ModelAttribute("user") User user, BindingResult result, Model model)
    {
        if(result.hasErrors())
        {
            List<Role> roles = mRoleService.getRoles();
            model.addAttribute("roles", roles);

            return "user-update-form";
        }

        user.setAccountPassword(user.getAccountPassword());
        mUserService.saveUser(user);

        return "redirect:/admin";
    }

    @GetMapping("/admin/deleteUser")
    public String deleteUser(@RequestParam("userId") int Id)
    {
        mUserService.deleteUser(Id);
        return "redirect:/admin";
    }

    @GetMapping("/admin/resetUserPassword")
    public String resetUserPassword(@RequestParam("userId") int Id)
    {
        User user = mUserService.getUser(Id);
        user.setAccountPassword(mBCryptPasswordEncoder.encode("haslo123"));
        mUserService.saveUser(user);
        return "redirect:/admin";
    }

    @GetMapping("/admin/showFormForUpdate")
    public String showFormForUpdate(@RequestParam("userId") int Id, Model theModel)
    {
        User user = mUserService.getUser(Id);
        theModel.addAttribute("user", user);
        List<Role> roles = mRoleService.getRoles();
        theModel.addAttribute("roles", roles);
        return "user-update-form";
    }

    @GetMapping("/admin/showFormForAddCard")
    public String showFormForAddCard(Model theModel)
    {
        // ewentualni użytkownicy, których można przypisać do kart
        List<User> users = mUserService.getUsersWithoutCard();
        theModel.addAttribute("usersWithoutCard", users);

        // lista kart bez użytkowników
        List<AccessCard> cards = mAccessCardService.getCardsWithoutUsers();
        theModel.addAttribute("cardsWithoutUsers", cards);
        // nowa karta do stworzenia

        AccessCard card = new AccessCard();
        theModel.addAttribute("card", card);
        return "card-form";
    }

    @PostMapping("/admin/saveCard")
    public String saveCard(@Valid @ModelAttribute("card") AccessCard accessCard, BindingResult result, Model model)
    {
        if(result.hasErrors())
        {
            List<User> users = mUserService.getUsersWithoutCard();
            model.addAttribute("usersWithoutCard", users);

            return "card-form";
        }

        if(accessCard.getUserByUserId().getUserId() == 0)
            accessCard.setUserByUserId(null);
        // zapisanie karty
        mAccessCardService.saveCard(accessCard);
        return "redirect:/admin";
    }

    @GetMapping("/admin/getUsersWithFirstNameAndLastName")
    @ResponseBody
    public String getUsersWithFirstNameAndLastName(String firstName, String lastName)
    {
        Long usersCount = mUserService.getUsersCountWithFirstNameAndLastName(firstName, lastName);
        Gson gson = new Gson();
        return gson.toJson(usersCount);
    }

    @GetMapping("/")
    public String showMyLoginPage()
    {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if(!(auth instanceof AnonymousAuthenticationToken))
        {
            return "redirect:/login";
        }
        return "plain-login";
    }

    //widok dla pracownikow
    @GetMapping("/employees")
    public String showEmployeePage()
    {
        return "home-employee";
    }

    //widok dla pracownikow
    @GetMapping("/managers")
    public String showManagerPage()
    {
        return "home-manager";
    }

    @GetMapping("/managers/showFormForAddTask")
    public String showFormForAddTask(Model theModel)
    {
        Task task = new Task();
        theModel.addAttribute("task", task);
        List<User> users = mUserService.getUsersLikeEmployee();
        theModel.addAttribute("users", users);
        return "task-form";
    }

    private void saveOrUpdateTask(Task task, HttpServletRequest request)
    {
        if(task.getUserByUserId().getUserId() == 0)
            task.setUserByUserId(null);
        if(request.isUserInRole("MANAGER"))
        {
            Authentication auth = SecurityContextHolder.getContext().getAuthentication();
            User user = mUserService.getUserByLogin(auth.getName());
            task.setUserByAuthorId(user);
            task.setShowed(false);
        }
        mTaskService.saveTask(task);
    }

    @PostMapping("/managers/updateTask")
    public String updateTask(@Valid @ModelAttribute("task") Task task, BindingResult result, Model model, HttpServletRequest request)
    {
        if(result.hasErrors())
        {
            model.addAttribute("states", mStateService.getStates());
            model.addAttribute("comments", mCommentService.getCommentsByTaskId(task.getTaskId()));
            model.addAttribute("users", mUserService.getUsersLikeEmployee());

            return "task-details";
        }

        saveOrUpdateTask(task, request);

        return "redirect:/managers/showTaskDetails?taskId=" + Integer.toString(task.getTaskId());
    }

    @PostMapping("/managers/saveTask")
    public String saveTask(@Valid @ModelAttribute("task") Task task, BindingResult result, Model model, HttpServletRequest request)
    {
        if(result.hasErrors())
        {
            List<User> users = mUserService.getUsersLikeEmployee();
            model.addAttribute("users", users);

            return "task-form";
        }

        saveOrUpdateTask(task, request);

        return "redirect:/managers";
    }

    @GetMapping("/managers/deleteTask")
    public String deleteTask(@RequestParam("taskId") int Id)
    {
        mTaskService.deleteTaskById(Id);
        return "redirect:/managers/taskList";
    }

    @GetMapping("/managers/showTaskDetails")
    public String showTaskDetailsForManager(@RequestParam("taskId") int id, Model theModel, HttpServletRequest request)
    {
        showTaskDetails(id, theModel, request);

        return "task-details";
    }

    @PostMapping("/managers/addComment")
    public String addComment(@Valid @ModelAttribute("comment") Comment comment, BindingResult result, Model model)
    {
        if(result.hasErrors())
        {
            model.addAttribute("task", mTaskService.getTaskById(comment.getTaskByTaskId().getTaskId()));
            model.addAttribute("states", mStateService.getStates());
            model.addAttribute("comments", mCommentService.getCommentsByTaskId(comment.getTaskByTaskId().getTaskId()));
            model.addAttribute("users", mUserService.getUsersLikeEmployee());
            return "task-details";
        }

        comment.setCommentDate(getCurrentDate());
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = mUserService.getUserByLogin(auth.getName());
        comment.setUserByUserId(user);
        mCommentService.saveComment(comment);
        return "redirect:/managers/showTaskDetails?taskId=" + Integer.toString(comment.getTaskByTaskId().getTaskId());
    }

    @GetMapping("/managers/leaveList")
    public String leaveList(Model theModel, Integer offset, Integer maxResults)
    {
        Leave leave = new Leave();
        theModel.addAttribute("leave", leave);
        theModel.addAttribute("leaves", mLeaveService.getAllLeaves(offset, maxResults));
        theModel.addAttribute("count", mLeaveService.count());
        theModel.addAttribute("offset", offset);
        return "leaves-form";
    }

    @GetMapping("/managers/getCountLeavesShowed")
    @ResponseBody
    public Long getCountLeavesShowed()
    {
        return mLeaveService.getCountLeavesShowed();
    }

    @GetMapping("/managers/getLeaveForUser")
    @ResponseBody
    public String getLeaveForUserByManager(String year)
    {
        return getLeaveForUser(year);
    }

    public String getLeaveForUser(String year)
    {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = mUserService.getUserByLogin(auth.getName());
        int theYear = Integer.parseInt(year);
        List<LeavesJson> leavesJson = mLeaveService.getLeaveForUser(user, theYear);
        Gson gson = new Gson();
        return gson.toJson(leavesJson);
    }

    @PostMapping("/managers/saveLeave")
    public String addLeaveByManager(@ModelAttribute("leave") Leave leave)
    {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = mUserService.getUserByLogin(auth.getName());
        State state = mStateService.getStateByStateId(8);
        leave.setStateByStateId(state);
        leave.setUserByUserId(user);
        mLeaveService.saveLeave(leave);
        return "redirect:/managers/leaveList";
    }

    @GetMapping("/managers/deleteLeave")
    public String deleteLeave(@RequestParam("leaveId") int Id)
    {
        mLeaveService.deleteLeave(Id);
        return "redirect:/managers/leaveList";
    }

    @GetMapping("/managers/acceptLeave")
    public String acceptLeave(@RequestParam("leaveId") int Id)
    {
        mLeaveService.acceptLeave(Id);
        return "redirect:/managers/leaveList";
    }

    @GetMapping("/managers/kickLeave")
    public String kickLeave(@RequestParam("leaveId") int Id)
    {
        mLeaveService.kickLeave(Id);
        return "redirect:/managers/leaveList";
    }

    @GetMapping("/managers/taskList")
    public String list(Model theModel, Integer offset, Integer maxResults)
    {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = mUserService.getUserByLogin(auth.getName());
        theModel.addAttribute("tasks", mTaskService.getListTask(user.getUserId(), offset, maxResults));
        theModel.addAttribute("count", mTaskService.count(user.getUserId()));
        theModel.addAttribute("offset", offset);
        return "task-list";
    }

    @GetMapping("/login")
    public String login(HttpServletRequest request)
    {
        if(request.isUserInRole("ADMIN"))
        {
            return "redirect:/admin";
        }
        else if(request.isUserInRole("MANAGER"))
        {
            return "redirect:/managers";
        }
        else
            return "redirect:/employees";
    }

    @ModelAttribute("scheduleForm")
    public ScheduleForm populateScheduleList()
    {
        ScheduleForm scheduleForm = new ScheduleForm();
        List<Schedule> scheduleList = new ArrayList<>();

        for(int i = 0; i < 5; i++)
            scheduleList.add(new Schedule());

        scheduleForm.setScheduleList(scheduleList);

        return scheduleForm;
    }

    @GetMapping("/employees/formForAddSchedule")
    public String showFormForAddEmployeeSchedule(@RequestParam("userId") int userId, Model theModel)
    {
        showFormForAddSchedule(userId, theModel);

        return "schedule-form";
    }

    @GetMapping("/employees/schedule")
    public String showEmployeeSchedule(Model model)
    {
        showSchedule(model);

        return "schedule";
    }

    @GetMapping("/managers/formForAddSchedule")
    public String showFormForAddManagerSchedule(@RequestParam("userId") int userId, Model theModel)
    {
        showFormForAddSchedule(userId, theModel);

        return "schedule-form";
    }

    @GetMapping("/managers/schedule")
    public String showManagerSchedule(Model model)
    {
        showSchedule(model);

        return "schedule";
    }

    @PostMapping("/employees/saveSchedule")
    public String saveEmployeeSchedule(@Valid @ModelAttribute("scheduleForm") ScheduleForm scheduleForm, BindingResult result, Model model)
    {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = mUserService.getUserByLogin(auth.getName());

        if(result.hasErrors())
        {
            List<WeekDay> workdays = mWeekDayService.getWorkdays();
            model.addAttribute("workdays", workdays);

            return "schedule-form";
        }

        saveSchedule(scheduleForm, user);

        return "redirect:/employees/schedule";
    }

    @PostMapping("/managers/saveSchedule")
    public String saveManagerSchedule(@Valid @ModelAttribute("scheduleForm") ScheduleForm scheduleForm, BindingResult result, Model model)
    {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = mUserService.getUserByLogin(auth.getName());

        if(result.hasErrors())
        {
            List<WeekDay> workdays = mWeekDayService.getWorkdays();
            model.addAttribute("workdays", workdays);

            return "schedule-form";
        }

        saveSchedule(scheduleForm, user);

        return "redirect:/managers/schedule";
    }

    private void saveSchedule(ScheduleForm scheduleForm, User user)
    {
        for(Schedule schedule : scheduleForm.getScheduleList())
        {
            schedule.setUserByUserId(user);

            mScheduleService.saveSchedule(schedule);
        }
    }

    private void showFormForAddSchedule(int userId, Model theModel)
    {
        ScheduleForm schedule = new ScheduleForm();
        List<Schedule> currentSchedule = mScheduleService.getScheduleByUserId(userId);

        if(!currentSchedule.isEmpty())
            schedule.setScheduleList(currentSchedule);

        theModel.addAttribute("scheduleForm", schedule);

        List<WeekDay> workdays = mWeekDayService.getWorkdays();
        theModel.addAttribute("workdays", workdays);
    }

    private void showSchedule(Model model)
    {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = mUserService.getUserByLogin(auth.getName());
        model.addAttribute("currentUser", user);
        Map<User, List<Schedule>> scheduleByUser = mScheduleService.getWholeSchedule();
        model.addAttribute("scheduleByUser", scheduleByUser);
    }

    @GetMapping("/admin/formForAddHistory")
    public String showFormForAddHistory(Model model)
    {
        History history = new History();
        model.addAttribute("history", history);
        return "history-form";
    }

    @PostMapping("/admin/saveHistory")
    public String saveHistory(@ModelAttribute("history") History history)
    {
        History lastHistoryForCardId = mHistoryService.getLastHistoryByAccessCardId(history.getAccessCardByAccessCardId().getCardId());

        if(lastHistoryForCardId == null
                || (lastHistoryForCardId.getEnterDate() != null && lastHistoryForCardId.getExitDate() != null))
            history.setEnterDate(getCurrentDate());
        else
        {
            history.setHistoryId(lastHistoryForCardId.getHistoryId());
            history.setEnterDate(lastHistoryForCardId.getEnterDate());
            history.setExitDate(getCurrentDate());
        }

        mHistoryService.saveHistory(history);
        return "redirect:/admin/formForAddHistory";
    }

    private Timestamp getCurrentDate()
    {
        return new Timestamp(System.currentTimeMillis());
    }

    @GetMapping("/employees/wholeHistoryForUser")
    public String showWholeHistoryForUser(Model model)
    {
        return "whole-history";
    }

    @GetMapping("/managers/wholeHistory")
    public String showWholeHistory(Model model)
    {
        List<User> employees = mUserService.getUsersLikeEmployee();
        model.addAttribute("employees", employees);

        return "whole-history";
    }

    @GetMapping("/historyJsonByUser")
    @ResponseBody
    public String getHistoryJsonByUser()
    {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = mUserService.getUserByLogin(auth.getName());

        List<HistoryJson> historyJson = mHistoryService.getHistoryJsonByUser(user.getUserId());

        Gson gson = new Gson();

        return gson.toJson(historyJson);
    }

    @GetMapping("/historyJsonForUser")
    @ResponseBody
    public String getHistoryJsonForUser(int userId)
    {
        List<HistoryJson> historyJson = mHistoryService.getHistoryJsonByUser(userId);

        Gson gson = new Gson();

        return gson.toJson(historyJson);
    }

    @GetMapping("/employees/taskList")
    public String taskList(Model theModel, Integer offset, Integer maxResults)
    {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = mUserService.getUserByLogin(auth.getName());
        theModel.addAttribute("tasks", mTaskService.getListTaskForEmployee(user.getUserId(), offset, maxResults));
        theModel.addAttribute("count", mTaskService.countForEmployee(user.getUserId()));
        theModel.addAttribute("offset", offset);
        return "task-list";
    }

    @GetMapping("/employees/getCountTaskShowed")
    @ResponseBody
    public Long getCountTaskShowed()
    {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = mUserService.getUserByLogin(auth.getName());
        return mTaskService.getCountTaskShowed(user.getUserId());
    }

    @GetMapping("/employees/showTaskDetails")
    public String showTaskDetailsForEmployees(@RequestParam("taskId") int id, Model theModel, HttpServletRequest request)
    {
        showTaskDetails(id, theModel, request);

        return "task-details";
    }

    private void showTaskDetails(int id, Model theModel, HttpServletRequest request)
    {
        Task task = mTaskService.getTaskById(id);
        if(request.isUserInRole("REGULAR_EMPLOYEE"))
        {
            task.setShowed(true);
            mTaskService.saveTask(task);
        }
        theModel.addAttribute("task", task);
        Comment comment = new Comment();
        comment.setTaskByTaskId(task);
        theModel.addAttribute("comment", comment);
        Collection<Comment> comments = mCommentService.getCommentsByTaskId(task.getTaskId());
        theModel.addAttribute("comments", comments);
        List<State> states = mStateService.getStates();
        theModel.addAttribute("states", states);
        User user = task.getUserByUserId();

        if(user != null)
        {
            List<User> users = mUserService.getUsersWithoutOne(user.getUserId());
            theModel.addAttribute("users", users);
        }
    }

    @PostMapping("/employees/updateTask")
    public String updateTaskByEmployee(@Valid @ModelAttribute("task") Task task, BindingResult result, Model model, HttpServletRequest request)
    {
        if(result.hasErrors())
        {
            model.addAttribute("states", mStateService.getStates());
            model.addAttribute("comments", mCommentService.getCommentsByTaskId(task.getTaskId()));
            model.addAttribute("users", mUserService.getUsersLikeEmployee());

            return "task-details";
        }

        saveOrUpdateTask(task, request);
        return "redirect:/employees/showTaskDetails?taskId=" + Integer.toString(task.getTaskId());
    }

    @PostMapping("/employees/addComment")
    public String addCommentByEmployee(@Valid @ModelAttribute("comment") Comment comment, BindingResult result, Model model)
    {
        if(result.hasErrors())
        {
            model.addAttribute("task", mTaskService.getTaskById(comment.getTaskByTaskId().getTaskId()));
            model.addAttribute("states", mStateService.getStates());
            model.addAttribute("comments", mCommentService.getCommentsByTaskId(comment.getTaskByTaskId().getTaskId()));
            model.addAttribute("users", mUserService.getUsersLikeEmployee());
            return "task-details";
        }

        comment.setCommentDate(getCurrentDate());
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = mUserService.getUserByLogin(auth.getName());
        comment.setUserByUserId(user);
        mCommentService.saveComment(comment);
        return "redirect:/employees/showTaskDetails?taskId=" + Integer.toString(comment.getTaskByTaskId().getTaskId());
    }

    @GetMapping("/employees/leaveList")
    public String leaveListForEmployee(Model theModel, Integer offset, Integer maxResults)
    {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = mUserService.getUserByLogin(auth.getName());
        Leave leave = new Leave();
        theModel.addAttribute("leave", leave);
        theModel.addAttribute("leaves", mLeaveService.getAllLeavesForEmployee(user.getUserId(), offset, maxResults));
        theModel.addAttribute("count", mLeaveService.countForEmployee(user.getUserId()));
        theModel.addAttribute("offset", offset);
        return "leaves-form";
    }

    @GetMapping("/employees/getLeaveForUser")
    @ResponseBody
    public String getLeaveForUserByEmployee(String year)
    {
        return getLeaveForUser(year);
    }

    @PostMapping("/employees/saveLeave")
    public String addLeave(@ModelAttribute("leave") Leave leave)
    {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = mUserService.getUserByLogin(auth.getName());
        leave.setUserByUserId(user);
        mLeaveService.saveLeave(leave);
        return "redirect:/employees/leaveList";
    }

    @GetMapping("/employees/deleteLeave")
    public String deleteLeaveByEmployee(@RequestParam("leaveId") int Id)
    {
        mLeaveService.deleteLeave(Id);
        return "redirect:/employees/leaveList";
    }

    @GetMapping("/leavesJsonForAllUsers")
    @ResponseBody
    public String getLeavesJsonForAllUsers()
    {
        List<LeavesJson> leavesJson = mLeaveService.getLeavesJsonForAllUsers();
        Gson gson = new Gson();
        return gson.toJson(leavesJson);
    }
}
