package com.spring.dao;

import com.spring.entity.User;

import java.util.List;

public interface UserDAO
{
    void saveUser(User user);

    List<User> getUsers(Integer offset, Integer maxResults);

    void deleteUser(int id);

    User getUser(int id);

    List<User> getUsersWithoutCard();

    User getUserByLogin(String login);

    List<User> getUsersLikeEmployee();

    List<User> getUsersWithoutOne(int id);

    Long getCount();

    Long getUsersCountWithFirstNameAndLastName(String firstName, String lastName);
}
