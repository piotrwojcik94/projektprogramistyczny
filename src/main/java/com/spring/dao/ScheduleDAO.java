package com.spring.dao;

import com.spring.entity.Schedule;
import com.spring.entity.User;

import java.util.List;
import java.util.Map;

public interface ScheduleDAO
{
    void saveSchedule(Schedule schedule);

    void saveWeekScheduleForUser(User user, List<Schedule> schedule);

    List<Schedule> getScheduleByUserId(int userId);

    Map<User, List<Schedule>> getWholeSchedule();
}
