package com.spring.dao;

import com.spring.entity.User;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class UserDAOImpl implements UserDAO
{

    @Autowired
    private SessionFactory mSessionFactory;

    @Override
    public void saveUser(User user)
    {
        Session currentSession = mSessionFactory.getCurrentSession();
        currentSession.saveOrUpdate(user);
    }

    @Override
    public List<User> getUsers(Integer offset, Integer maxResults)
    {
        Session currentSession = mSessionFactory.getCurrentSession();
        Criteria criteria = currentSession.createCriteria(User.class)
                            .addOrder(Order.asc("lastName"))
                            .setFirstResult(offset!=null?offset:0)
                            .setMaxResults(maxResults!=null?maxResults:10);
        return (List<User>) criteria.list();
    }

    @Override
    public void deleteUser(int id)
    {
        Session currentSession = mSessionFactory.getCurrentSession();
        Query query = currentSession.createQuery("delete from User where userId=:userId");
        query.setParameter("userId", id);
        query.executeUpdate();
    }

    @Override
    public User getUser(int id){

        Session currentSession = mSessionFactory.getCurrentSession();
        User user = currentSession.get(User.class, id);
        return user;
    }

    @Override
    public List<User> getUsersWithoutCard() {

        Session currentSession = mSessionFactory.getCurrentSession();
        Query query = currentSession.createQuery("FROM User u LEFT JOIN u.accessCardByUserId ac WHERE ac.userByUserId.userId is NULL AND u.roleByRoleId.role NOT LIKE 'ADMIN'");

        List<User> users = new ArrayList<>();

        //noinspection unchecked
        List<Object[]> objects = (List<Object[]>) query.list();

        for(Object[] object : objects)
            users.add((User) object[0]);

        return users;
    }

    @Override
    public User getUserByLogin(String login){

        Session currentSession = mSessionFactory.getCurrentSession();
        Query query = currentSession.createQuery("FROM User u WHERE u.accountLogin = :login");
        query.setParameter("login", login);

        User user = (User) query.uniqueResult();
        return user;
    }

    @Override
    public List<User> getUsersLikeEmployee() {

        Session currentSession = mSessionFactory.getCurrentSession();
        Query query = currentSession.createQuery("FROM User u WHERE u.roleByRoleId.id = :roleId");
        query.setParameter("roleId",2);

        List<User> users = query.list();
        return users;
    }

    @Override
    public List<User> getUsersWithoutOne(int id) {
        Session currentSession = mSessionFactory.getCurrentSession();
        Query query = currentSession.createQuery("FROM User where userId!=:idU AND roleByRoleId.roleId=2");
        query.setParameter("idU",id);
        List<User> users = query.list();
        return users;
    }

    @Override
    public Long getCount() {
        Session currentSession = mSessionFactory.getCurrentSession();
        return (Long) currentSession.createCriteria(User.class).setProjection(Projections.rowCount()).uniqueResult();
    }

    @Override
    public Long getUsersCountWithFirstNameAndLastName(String firstName, String lastName) {
        Session currentSession = mSessionFactory.getCurrentSession();
        return (Long) currentSession.createCriteria(User.class).add(Restrictions.like("firstName",firstName)).add(Restrictions.like("lastName",lastName)).setProjection(Projections.rowCount()).uniqueResult();
    }
}
