package com.spring.dao;

import com.spring.entity.Role;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class RoleDAOImpl implements RoleDAO
{

    @Autowired
    private SessionFactory mSessionFactory;

    @Override
    public void saveRole(Role role)
    {
        Session currentSession = mSessionFactory.getCurrentSession();
        currentSession.saveOrUpdate(role);
    }

    @Override
    public List<Role> getRoles()
    {
        Session currentSession = mSessionFactory.getCurrentSession();
        Query query =
                currentSession.createQuery("from Role order by role");
        List<Role> roles = query.list();
        return roles;
    }

    @Override
    public void deleteRole(int id)
    {
        Session currentSession = mSessionFactory.getCurrentSession();
        Query query = currentSession.createQuery("delete from Role where roleId=:roleId");
        query.setParameter("roleId", id);
        query.executeUpdate();
    }

    @Override
    public Role getRole(int id)
    {
        Session currentSession = mSessionFactory.getCurrentSession();
        Role role = currentSession.get(Role.class, id);
        return role;
    }
}
