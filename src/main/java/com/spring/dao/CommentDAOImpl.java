package com.spring.dao;

import com.spring.entity.Comment;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CommentDAOImpl implements CommentDAO {

    @Autowired
    private SessionFactory mSessionFactory;

    @Override
    public List<Comment> getCommentByTaskId(int id) {
        Session currentSession = mSessionFactory.getCurrentSession();
        Query query = currentSession.createQuery("FROM Comment where taskByTaskId.taskId=:id");
        query.setParameter("id",id);
        List<Comment> comments = query.list();
        return comments;
    }

    @Override
    public void saveComment(Comment comment) {
        Session currentSession = mSessionFactory.getCurrentSession();
        currentSession.saveOrUpdate(comment);
    }
}
