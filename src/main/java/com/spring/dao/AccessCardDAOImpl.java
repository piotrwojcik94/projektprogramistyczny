package com.spring.dao;

import com.spring.entity.AccessCard;
import com.spring.entity.User;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class AccessCardDAOImpl implements AccessCardDAO {

    @Autowired
    private SessionFactory mSessionFactory;

    @Override
    public List<AccessCard> getAccessCards() {
//        Session currentSession = mSessionFactory.getCurrentSession();
//        Query query = currentSession.createQuery("from AccessCard ac where ac.userByUserId.userId is null and ac.userByUserId.roleByRoleId.role not like 'ADMIN'");
//        List<AccessCard> cards = query.list();
        return null;
    }

    @Override
    public void addUserToCard(User user) {

    }

    @Override
    public AccessCard getCard(int id) {
        Session currentSession = mSessionFactory.getCurrentSession();
        AccessCard card = currentSession.get(AccessCard.class, id);
        return card;
    }

    @Override
    public void saveCard(AccessCard accessCard) {
        Session currentSession = mSessionFactory.getCurrentSession();
        currentSession.saveOrUpdate(accessCard);
    }

    @Override
    public List<AccessCard> getCardsWithoutUsers() {
        Session currentSession = mSessionFactory.getCurrentSession();
        Query query = currentSession.createQuery("from AccessCard where userByUserId.userId = null");
        return query.list();
    }
}
