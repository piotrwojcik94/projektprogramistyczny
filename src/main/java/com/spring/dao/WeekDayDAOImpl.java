package com.spring.dao;

import com.spring.entity.WeekDay;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class WeekDayDAOImpl implements WeekDayDAO
{
    @Autowired
    private SessionFactory mSessionFactory;

    @Override
    public List<WeekDay> getWorkdays()
    {
        Session currentSession = mSessionFactory.getCurrentSession();
        Query query = currentSession.createQuery("from WeekDay where dayId between 1 and 5");

        //noinspection unchecked
        List<WeekDay> workdays = (List<WeekDay>) query.list();

        return workdays;
    }
}
