package com.spring.dao;

import com.spring.entity.State;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class StateDAOImpl implements StateDAO {

    @Autowired
    private SessionFactory mSessionFactory;

    @Override
    public List<State> getState() {
        Session currentSession = mSessionFactory.getCurrentSession();
        Query query = currentSession.createQuery("from State where stateId between 1 and 5 order by state");
        List<State> states = query.list();
        return states;
    }

    @Override
    public State getStateByStateId(int id) {
        Session currentSession = mSessionFactory.getCurrentSession();
        State state = currentSession.get(State.class, id);
        return state;
    }
}
