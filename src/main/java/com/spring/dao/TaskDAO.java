package com.spring.dao;

import com.spring.entity.Task;
import com.spring.entity.User;

import java.util.List;

public interface TaskDAO {

    void saveTask(Task task);

    List<Task> getTaskByManager(User user);

    Task getTaskById(int id);

    List<Task> getTaskForEmployee(User user);

    List<Task> getListTask(int userId, Integer offset, Integer maxResults);

    Long count(int userId);

    void deleteTaskById(int id);

    List<Task> getListTaskForEmployee(int userId, Integer offset, Integer maxResults);

    Long countForEmployee(int userId);

    Long getCountTaskShowed(int userId);
}
