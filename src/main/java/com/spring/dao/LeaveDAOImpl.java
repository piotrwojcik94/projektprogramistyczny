package com.spring.dao;

import com.spring.entity.Leave;
import com.spring.entity.LeavesJson;
import com.spring.entity.User;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class LeaveDAOImpl implements LeaveDAO {

    @Autowired
    private SessionFactory mSessionFactory;

    @Override
    public List<Leave> getLeavesByUserId(int userId) {
        Session currentSession = mSessionFactory.getCurrentSession();
        Query query = currentSession.createQuery("from Leave where userByUserId.userId=:userId");
        query.setParameter("userId",userId);
        List<Leave> leaves = query.list();
        return leaves;
    }

    @Override
    public void saveLeave(Leave leave) {
        Session currentSession = mSessionFactory.getCurrentSession();
        currentSession.saveOrUpdate(leave);
    }

    @Override
    public List<LeavesJson> getLeavesJsonForAllUsers() {
        Session currentSession = mSessionFactory.getCurrentSession();
        Query query = currentSession.createQuery("select new com.spring.entity.LeavesJson(" +
                                                    "concat(userByUserId.firstName, ' ', userByUserId.lastName), " +
                                                    "date_format(dateFrom, '%Y-%m-%d'), " +
                                                    "date_format(dateTo, '%Y-%m-%d'), " +
                                                    "concat(stateByStateId.stateId,'')" +
                                                    ") " +
                                                    "from Leave");
        List<LeavesJson> leaves = (List<LeavesJson>) query.list();
        return leaves;
    }

    @Override
    public List<LeavesJson> getLeaveForUser(User user, int year) {
        int userId = user.getUserId();
        Session currentSession = mSessionFactory.getCurrentSession();
        Query query = currentSession.createQuery("select new com.spring.entity.LeavesJson(" +
                                                    "concat(userByUserId.firstName, ' ', userByUserId.lastName), " +
                                                    "date_format(dateFrom, '%Y-%m-%d'), " +
                                                    "date_format(dateTo, '%Y-%m-%d')," +
                                                    "concat(stateByStateId.stateId,'')" +
                                                    ") " +
                                                    "from Leave where userByUserId.userId=:userId and year(dateFrom)=:year and stateByStateId.stateId!=7");
        query.setParameter("userId",userId);
        query.setParameter("year",year);
        List<LeavesJson> leaves = (List<LeavesJson>) query.list();
        return leaves;
    }

    @Override
    public List<Leave> getAllLeavesForEmployee(int userId, Integer offset, Integer maxResults) {
        Session currentSession = mSessionFactory.getCurrentSession();
        Criteria criteria = currentSession.createCriteria(Leave.class)
                            .setFirstResult(offset!=null?offset:0)
                            .setMaxResults(maxResults!=null?maxResults:10)
                            .add(Restrictions.like("userByUserId.userId",userId));
        return (List<Leave>) criteria.list();
    }

    @Override
    public Long countForEmployee(int userId) {
        Session currentSession = mSessionFactory.getCurrentSession();
        return (Long) currentSession.createCriteria(Leave.class).setProjection(Projections.rowCount()).add(Restrictions.like("userByUserId.userId",userId)).uniqueResult();
    }

    @Override
    public Long getCountLeavesShowed() {
        Session currentSession = mSessionFactory.getCurrentSession();
        return (Long) currentSession.createCriteria(Leave.class).setProjection(Projections.rowCount()).add(Restrictions.like("stateByStateId.stateId",6)).uniqueResult();
    }

    @Override
    public List<Leave> getAllLeaves(Integer offset, Integer maxResults) {
        Session currentSession = mSessionFactory.getCurrentSession();
        Criteria criteria = currentSession.createCriteria(Leave.class)
                            .setFirstResult(offset!=null?offset:0)
                            .setMaxResults(maxResults!=null?maxResults:10);
        return (List<Leave>) criteria.list();
    }

    @Override
    public void deleteLeave(int id) {
        Session currentSession = mSessionFactory.getCurrentSession();
        Query query = currentSession.createQuery("delete from Leave where leaveId=:leaveId");
        query.setParameter("leaveId",id);
        query.executeUpdate();
    }

    @Override
    public void acceptLeave(int id) {
        Session currentSession = mSessionFactory.getCurrentSession();
        Query query = currentSession.createQuery("update Leave set stateByStateId.stateId=8 where leaveId=:leaveId");
        query.setParameter("leaveId",id);
        query.executeUpdate();
    }

    @Override
    public void kickLeave(int id) {
        Session currentSession = mSessionFactory.getCurrentSession();
        Query query = currentSession.createQuery("update Leave set stateByStateId.stateId=7 where leaveId=:leaveId");
        query.setParameter("leaveId",id);
        query.executeUpdate();
    }

    @Override
    public Long count() {
        Session currentSession = mSessionFactory.getCurrentSession();
        return (Long) currentSession.createCriteria(Leave.class).setProjection(Projections.rowCount()).uniqueResult();
    }

}
