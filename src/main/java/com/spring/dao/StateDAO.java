package com.spring.dao;

import com.spring.entity.State;

import java.util.List;

public interface StateDAO {
    List<State> getState();

    State getStateByStateId(int id);
}
