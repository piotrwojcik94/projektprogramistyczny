package com.spring.dao;

import com.spring.entity.AccessCard;
import com.spring.entity.User;

import java.util.List;

public interface AccessCardDAO {
    List<AccessCard> getAccessCards();

    void addUserToCard(User user);

    AccessCard getCard(int id);

    void saveCard(AccessCard accessCard);

    List<AccessCard> getCardsWithoutUsers();
}
