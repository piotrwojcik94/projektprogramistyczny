package com.spring.dao;


import com.spring.entity.Comment;

import java.util.List;

public interface CommentDAO {
    List<Comment> getCommentByTaskId(int id);

    void saveComment(Comment comment);
}
