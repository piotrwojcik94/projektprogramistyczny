package com.spring.dao;

import com.spring.entity.History;
import com.spring.entity.HistoryJson;
import com.spring.entity.User;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Repository
public class HistoryDAOImpl implements HistoryDAO
{
    @Autowired
    private SessionFactory mSessionFactory;

    @Override
    public void saveHistory(History history)
    {
        Session currentSession = mSessionFactory.getCurrentSession();
        currentSession.saveOrUpdate(history);
    }

    @Override
    public Map<User, List<History>> getWholeHistory()
    {
        Session currentSession = mSessionFactory.getCurrentSession();
        Query query = currentSession.createQuery(
                "from History h " +
                        "join h.accessCardByAccessCardId.userByUserId u " +
                        "order by u.lastName");

        List<History> history = new ArrayList<>();

        //noinspection unchecked
        List<Object[]> objects = (List<Object[]>) query.list();

        for(Object[] object : objects)
            history.add((History) object[0]);

        Map<User, List<History>> historyByUser = history.stream().collect(
                Collectors.groupingBy(his -> his.getAccessCardByAccessCardId().getUserByUserId()));

        return historyByUser;
    }

    @Override
    public List<History> getHistoryByUser(int userId)
    {
        Session currentSession = mSessionFactory.getCurrentSession();
        Query query = currentSession.createQuery(
                "from History where accessCardByAccessCardId.userByUserId.userId = :userId");
        query.setParameter("userId", userId);

        //noinspection unchecked
        List<History> history = (List<History>) query.list();

        return history;
    }

    @Override
    public List<HistoryJson> getHistoryJsonByUser(int userId)
    {
        Session currentSession = mSessionFactory.getCurrentSession();
        Query query = currentSession.createQuery(
                "select new com.spring.entity.HistoryJson(" +
                            "concat(date_format(enterDate, '%H:%i:%s'), ' - ', date_format(exitDate, '%H:%i:%s')), " +
                            "date_format(enterDate, '%Y-%m-%d %H:%i:%s'), " +
                            "date_format(exitDate, '%Y-%m-%d %H:%i:%s'), " +
                            "date_format(timediff(date_format(exitDate, '%Y-%m-%d %H:%i:%s'), date_format(enterDate, '%Y-%m-%d %H:%i:%s')), '%H:%i:%s')" +
                        ") " +
                        "from History " +
                        "where accessCardByAccessCardId.userByUserId.userId = :userId");
//        query.setResultTransformer(Transformers.aliasToBean(HistoryJson.class));
        query.setParameter("userId", userId);

        //noinspection unchecked
        List<HistoryJson> history = (List<HistoryJson>) query.list();

        return history;
    }

    @Override
    public History getLastHistoryByAccessCardId(int accessCardId)
    {
        Session currentSession = mSessionFactory.getCurrentSession();

        Query query = currentSession.createQuery(
                "from History " +
                        "where accessCardByAccessCardId.cardId = :cardId " +
                        "order by historyId DESC");
        query.setParameter("cardId", accessCardId);
        query.setMaxResults(1);

        History history = (History) query.uniqueResult();

        return history;
    }
}
