package com.spring.dao;

import com.spring.entity.WeekDay;

import java.util.List;

public interface WeekDayDAO
{
    List<WeekDay> getWorkdays();
}
