package com.spring.dao;

import com.spring.entity.Task;
import com.spring.entity.User;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class TaskDAOImpl implements TaskDAO {

    @Autowired
    SessionFactory mSessionFactory;

    @Override
    public void saveTask(Task task) {
        Session currentSession = mSessionFactory.getCurrentSession();
        currentSession.saveOrUpdate(task);
    }

    @Override
    public List<Task> getTaskByManager(User user) {
        Session currentSession = mSessionFactory.getCurrentSession();
        Query query = currentSession.createQuery("from Task where userByAuthorId=:user");
        query.setParameter("user",user);
        List<Task> tasks = query.list();
        return tasks;
    }

    @Override
    public Task getTaskById(int id) {
        Session currentSession = mSessionFactory.getCurrentSession();
        Query query = currentSession.createQuery("from Task where taskId=:id");
        query.setParameter("id",id);
        Task task = (Task) query.uniqueResult();
        return task;
    }

    @Override
    public List<Task> getTaskForEmployee(User user) {
        Session currentSession = mSessionFactory.getCurrentSession();
        Query query = currentSession.createQuery("from Task where userByUserId=:user");
        query.setParameter("user",user);
        List<Task> tasks = query.list();
        return tasks;
    }

    @Override
    public List<Task> getListTask(int userId, Integer offset, Integer maxResults) {
        Session currentSession = mSessionFactory.getCurrentSession();
        Criteria criteria = currentSession.createCriteria(Task.class)
                            .add(Restrictions.like("userByAuthorId.userId",userId))
                            .addOrder(Order.asc("startDate"))
                            .setFirstResult(offset!=null?offset:0)
                            .setMaxResults(maxResults!=null?maxResults:10);
        return (List<Task>) criteria.list();
    }

    @Override
    public Long count(int userId) {
        Session currentSession = mSessionFactory.getCurrentSession();
        return (Long) currentSession.createCriteria(Task.class).add(Restrictions.like("userByAuthorId.userId",userId)).setProjection(Projections.rowCount()).uniqueResult();
    }

    @Override
    public void deleteTaskById(int id) {
        Session currentSession = mSessionFactory.getCurrentSession();
        Query query = currentSession.createQuery("delete from Task where taskId=:taskId");
        query.setParameter("taskId",id);
        query.executeUpdate();
    }

    @Override
    public List<Task> getListTaskForEmployee(int userId, Integer offset, Integer maxResults) {
        Session currentSession = mSessionFactory.getCurrentSession();
        Criteria criteria = currentSession.createCriteria(Task.class)
                            .add(Restrictions.like("userByUserId.userId",userId))
                            .setFirstResult(offset!=null?offset:0)
                            .setMaxResults(maxResults!=null?maxResults:10);
        return (List<Task>) criteria.list();
    }

    @Override
    public Long countForEmployee(int userId) {
        Session currentSession = mSessionFactory.getCurrentSession();
        return (Long) currentSession.createCriteria(Task.class).setProjection(Projections.rowCount()).add(Restrictions.like("userByUserId.userId",userId)).uniqueResult();
    }

    @Override
    public Long getCountTaskShowed(int userId) {
        Session currentSession = mSessionFactory.getCurrentSession();
        return (Long) currentSession.createCriteria(Task.class).setProjection(Projections.rowCount()).add(Restrictions.like("showed",false)).add(Restrictions.like("userByUserId.userId",userId)).uniqueResult();
    }


}
