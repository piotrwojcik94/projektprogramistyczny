package com.spring.dao;

import com.spring.entity.Role;

import java.util.List;

public interface RoleDAO
{
    void saveRole(Role role);

    List<Role> getRoles();

    void deleteRole(int id);

    Role getRole(int id);
}
