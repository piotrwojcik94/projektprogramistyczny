package com.spring.dao;

import com.spring.entity.Schedule;
import com.spring.entity.User;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Repository
public class ScheduleDAOImpl implements ScheduleDAO
{
    @Autowired
    private SessionFactory mSessionFactory;

    @Override
    public void saveSchedule(Schedule schedule)
    {
        Session currentSession = mSessionFactory.getCurrentSession();
        currentSession.saveOrUpdate(schedule);
    }

    @Override
    public void saveWeekScheduleForUser(User user, List<Schedule> schedules)
    {
        Session currentSession = mSessionFactory.getCurrentSession();

        for(Schedule schedule : schedules)
        {
            schedule.setUserByUserId(user);
            currentSession.saveOrUpdate(schedule);
        }
    }

    @Override
    public List<Schedule> getScheduleByUserId(int userId)
    {
        Session currentSession = mSessionFactory.getCurrentSession();
        Query query = currentSession.createQuery(
                "from Schedule s " +
                        "right join s.weekDayByWeekDayId wd " +
                        "left join s.userByUserId u " +
                        "where wd.dayId between 1 and 5 " +
                        "and u.userId = :userId " +
                        "order by wd.dayId");
        query.setParameter("userId", userId);

        List<Schedule> schedule = new ArrayList<>();

        //noinspection unchecked
        List<Object[]> objects = (List<Object[]>) query.list();

        for(Object[] object : objects)
            schedule.add((Schedule) object[0]);

        return schedule;
    }

    @Override
    public Map<User, List<Schedule>> getWholeSchedule()
    {
        Session currentSession = mSessionFactory.getCurrentSession();
        Query query = currentSession.createQuery("from Schedule s right join s.weekDayByWeekDayId wd left join s.userByUserId u where wd.dayId between 1 and 5 order by wd.dayId");

        List<Schedule> schedule = new ArrayList<>();

        //noinspection unchecked
        List<Object[]> objects = (List<Object[]>) query.list();

        for(Object[] object : objects)
        {
            if(object[0] == null)
                return Collections.emptyMap();

            schedule.add((Schedule) object[0]);
        }

        Map<User, List<Schedule>> scheduleByUser = schedule.stream()
                .collect(Collectors.groupingBy(Schedule::getUserByUserId));

        return scheduleByUser;
    }


}
